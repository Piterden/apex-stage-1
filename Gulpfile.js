/*eslint semi: [1, always]*/

var
  appReference = 'apex',
  namespace = 'defr',
  themeName = 'apex',
  resPath = `addons/${appReference}/${namespace}/${themeName}-theme/resources/`,
  Elixir = require('laravel-elixir');

require('laravel-elixir-webpack-official');
require('laravel-elixir-vue-2');
require('url-loader');

Elixir.config.sourcemaps = true;

Elixir.webpack.mergeConfig({
  babel: {
    presets: ['es2015', 'stage-2'],
    plugins: ['transform-runtime'],
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    }
  },
  module: {
    loaders: [
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      {
        test: /\.(scss|sass)$/,
        loader: 'style!css!sass',
      },
      {
        test: /\.styl(us)?$/,
        loader: 'style!css!stylus',
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file?name=assets/[name].[ext]'
      },
    ]
  },
});

/* eslint-disable no-console */
// console.log(Elixir.webpack.config.module.loaders);
/* eslint-enable no-console */

Elixir(function (mix) {
  mix
    .copy(`${resPath}img`, 'public/img')
    .copy(`${resPath}css/materialize.css`, 'public/css/materialize.css')
    // .sass(`../../../${resPath}sass/app.scss`)
    .stylus(`../../../${resPath}stylus/app.styl`)
    // .webpack('vendor.js', 'public/js', `${resPath}js`)
    .webpack('main.js', 'public/js', `${resPath}js`)
    /* .browserSync({
      proxy: 'apex.web812.pro'
    }) */
  ;
});
