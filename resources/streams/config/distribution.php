<?php

return [
    'name'        => 'PyroCMS',
    'description' => 'The most powerful Laravel CMS in the world.',
    'version'     => 'v3.3.0',
];
