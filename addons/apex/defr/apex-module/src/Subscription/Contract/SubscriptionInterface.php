<?php namespace Defr\ApexModule\Subscription\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface SubscriptionInterface extends EntryInterface
{

}
