<?php namespace Defr\ApexModule\Subscription\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface SubscriptionRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find subscription by email
     *
     * @param  string                  $email The email
     * @return SubscriptionInterface
     */
    public function findByEmail($email);
}
