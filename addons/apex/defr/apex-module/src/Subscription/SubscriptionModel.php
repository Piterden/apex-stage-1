<?php namespace Defr\ApexModule\Subscription;

use Defr\ApexModule\Subscription\Contract\SubscriptionInterface;
use Anomaly\Streams\Platform\Model\Apex\ApexSubscriptionsEntryModel;

class SubscriptionModel extends ApexSubscriptionsEntryModel implements SubscriptionInterface
{

}
