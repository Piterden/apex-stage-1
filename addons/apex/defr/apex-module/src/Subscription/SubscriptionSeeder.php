<?php namespace Defr\ApexModule\Subscription;

use Defr\ApexModule\Subscription\Contract\SubscriptionRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Illuminate\Contracts\Config\Repository;

class SubscriptionSeeder extends Seeder
{

    /**
     * The Subscription repository.
     *
     * @var SubscriptionRepositoryInterface
     */
    protected $subscriptions;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a new SubscriptionSeeder instance.
     *
     * @param Repository $config
     * @param SubscriptionRepositoryInterface $subscriptions
     */
    public function __construct(
        Repository $config,
        SubscriptionRepositoryInterface $subscriptions
    )
    {
        $this->config = $config;
        $this->subscriptions = $subscriptions;
    }

    /**
     * Run the seeder
     */
    public function run()
    {

    }
}
