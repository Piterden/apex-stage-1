<?php namespace Defr\ApexModule\Subscription;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\ApexModule\Subscription\Contract\SubscriptionRepositoryInterface;

class SubscriptionRepository extends EntryRepository implements SubscriptionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var SubscriptionModel
     */
    protected $model;

    /**
     * Create a new SubscriptionRepository instance.
     *
     * @param SubscriptionModel $model
     */
    public function __construct(SubscriptionModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find subscription by email
     *
     * @param string $email The email
     */
    public function findByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }
}
