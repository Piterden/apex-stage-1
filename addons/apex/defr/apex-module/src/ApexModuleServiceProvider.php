<?php namespace Defr\ApexModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Apex\ApexBannersEntryModel;
use Anomaly\Streams\Platform\Model\Apex\ApexQuestionsEntryModel;
use Anomaly\Streams\Platform\Model\Apex\ApexRecallsEntryModel;
use Anomaly\Streams\Platform\Model\Apex\ApexReviewsEntryModel;
use Anomaly\Streams\Platform\Model\Apex\ApexSubscriptionsEntryModel;
use Defr\ApexModule\Banner\BannerModel;
use Defr\ApexModule\Banner\BannerRepository;
use Defr\ApexModule\Banner\Contract\BannerRepositoryInterface;
use Defr\ApexModule\Question\Contract\QuestionRepositoryInterface;
use Defr\ApexModule\Question\QuestionModel;
use Defr\ApexModule\Question\QuestionRepository;
use Defr\ApexModule\Recall\Contract\RecallRepositoryInterface;
use Defr\ApexModule\Recall\RecallModel;
use Defr\ApexModule\Recall\RecallRepository;
use Defr\ApexModule\Review\Contract\ReviewRepositoryInterface;
use Defr\ApexModule\Review\ReviewModel;
use Defr\ApexModule\Review\ReviewRepository;
use Defr\ApexModule\Subscription\Contract\SubscriptionRepositoryInterface;
use Defr\ApexModule\Subscription\SubscriptionModel;
use Defr\ApexModule\Subscription\SubscriptionRepository;

class ApexModuleServiceProvider extends AddonServiceProvider
{

    protected $bindings = [
        ApexBannersEntryModel::class       => BannerModel::class,
        ApexQuestionsEntryModel::class     => QuestionModel::class,
        ApexReviewsEntryModel::class       => ReviewModel::class,
        ApexRecallsEntryModel::class       => RecallModel::class,
        ApexSubscriptionsEntryModel::class => SubscriptionModel::class,
    ];

    protected $singletons = [
        BannerRepositoryInterface::class       => BannerRepository::class,
        QuestionRepositoryInterface::class     => QuestionRepository::class,
        ReviewRepositoryInterface::class       => ReviewRepository::class,
        RecallRepositoryInterface::class       => RecallRepository::class,
        SubscriptionRepositoryInterface::class => SubscriptionRepository::class,
    ];

    protected $routes = [
        'admin/apex'                         => 'Defr\ApexModule\Http\Controller\Admin\BannersController@index',
        'admin/apex/create'                  => 'Defr\ApexModule\Http\Controller\Admin\BannersController@create',
        'admin/apex/edit/{id}'               => 'Defr\ApexModule\Http\Controller\Admin\BannersController@edit',
        'admin/apex/questions'               => 'Defr\ApexModule\Http\Controller\Admin\QuestionsController@index',
        'admin/apex/questions/create'        => 'Defr\ApexModule\Http\Controller\Admin\QuestionsController@create',
        'admin/apex/questions/edit/{id}'     => 'Defr\ApexModule\Http\Controller\Admin\QuestionsController@edit',
        'admin/apex/recalls'                 => 'Defr\ApexModule\Http\Controller\Admin\RecallsController@index',
        'admin/apex/recalls/create'          => 'Defr\ApexModule\Http\Controller\Admin\RecallsController@create',
        'admin/apex/recalls/edit/{id}'       => 'Defr\ApexModule\Http\Controller\Admin\RecallsController@edit',
        'admin/apex/reviews'                 => 'Defr\ApexModule\Http\Controller\Admin\ReviewsController@index',
        'admin/apex/reviews/create'          => 'Defr\ApexModule\Http\Controller\Admin\ReviewsController@create',
        'admin/apex/reviews/edit/{id}'       => 'Defr\ApexModule\Http\Controller\Admin\ReviewsController@edit',
        'admin/apex/subscriptions'           => 'Defr\ApexModule\Http\Controller\Admin\SubscriptionsController@index',
        'admin/apex/subscriptions/create'    => 'Defr\ApexModule\Http\Controller\Admin\SubscriptionsController@create',
        'admin/apex/subscriptions/edit/{id}' => 'Defr\ApexModule\Http\Controller\Admin\SubscriptionsController@edit',
    ];
}
