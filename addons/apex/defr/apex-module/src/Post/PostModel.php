<?php namespace Defr\ApexModule\Post;

/**
 * PostModel class
 *
 * @package default
 * @author
 */
class PostModel extends \Anomaly\PostsModule\Post\PostModel implements \Defr\ApexModule\Post\Contract\PostInterface
{

    /**
     * Gets the featured.
     *
     * @return bool The featured.
     */
    public function getFeatured()
    {
        return $this->featured;
    }

    /**
     * Sets the featured.
     *
     * @param  bool   $featured The featured
     * @return bool
     */
    public function setFeatured($featured)
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl()
    {
        return $this->getRelatedImageUrl('image');
    }

    /**
     * Gets the related image url.
     *
     * @param  string $fieldName The field name
     * @return string The related image url.
     */
    public function getRelatedImageUrl($fieldName)
    {
        if ($this->$fieldName)
        {
            $filename   = $this->$fieldName->name;
            $foldername = strtolower($this->$fieldName->folder->name);
            $diskname   = strtolower($this->$fieldName->disk->name);

            return url("app/apex/files-module/{$diskname}/{$foldername}/{$filename}");
        }

        return '';
    }

}
