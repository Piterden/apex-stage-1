<?php namespace Defr\ApexModule\Post;

use Carbon\Carbon;
use Illuminate\Config\Repository;
use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;
use Anomaly\PostsModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Model\Posts\PostsDefaultPostsEntryModel;
use Anomaly\PostsModule\Category\Contract\CategoryRepositoryInterface;

/**
 * Class PostSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class PostSeeder extends Seeder
{

    /**
     * The post repository.
     *
     * @var PostRepositoryInterface
     */
    protected $posts;

    /**
     * The type repository.
     *
     * @var TypeRepositoryInterface
     */
    protected $types;

    /**
     * The config repository.
     *
     * @var Illuminate\Config\Repository
     */
    protected $config;

    /**
     * The category repository.
     *
     * @var CategoryRepositoryInterface
     */
    protected $categories;

    /**
     * Create a new PostSeeder instance.
     *
     * @param Repository                  $config
     * @param PostRepositoryInterface     $posts
     * @param TypeRepositoryInterface     $types
     * @param CategoryRepositoryInterface $categories
     */
    public function __construct(
        Repository $config,
        PostRepositoryInterface $posts,
        TypeRepositoryInterface $types,
        CategoryRepositoryInterface $categories
    )
    {
        $this->posts      = $posts;
        $this->types      = $types;
        $this->config     = $config;
        $this->categories = $categories;
    }

    /**
     * Run the seeder
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting posts seeder!\n";

        $this->posts->truncate();

        $repository = new EntryRepository();
        $repository->setModel(new PostsDefaultPostsEntryModel());
        $repository->truncate();

        echo "\033[35;5;228mPosts cleared!\n";

        if ($data = file_get_contents(__DIR__.'/../../resources/seeders/posts_1.json'))
        {
            $posts = collect(json_decode($data, true))->groupBy('locale_id');

            $posts->each(function ($post) use ($repository)
            {
                $byLocale = $post->mapWithKeys(
                    function ($trans)
                    {
                        return [
                            array_get($trans, 'lang') => $trans,
                        ];
                    }
                );

                $ru = collect($byLocale->get('ru'));
                $en = collect($byLocale->get('en'));

                $type     = $this->types->findBySlug('default');
                $category = $this->categories->findBySlug('news');

                $entry = $repository->create([
                    'en' => [
                        'content' => $en->get('content'),
                    ],
                    'ru' => [
                        'content' => $ru->get('content'),
                    ],
                ]);

                $now  = new Carbon($en->get('created_at'));
                $slug = str_slug($en->get('title'));
                $i    = 1;

                while ($this->posts->findBySlug($slug))
                {
                    $slug = $slug.'-'.$i;
                    ++$i;
                }

                $this->posts->create(
                    [
                        'en'         => [
                            'title'   => $en->get('title'),
                            'summary' => $en->get('summary'),
                        ],
                        'ru'         => [
                            'title'   => $ru->get('title'),
                            'summary' => $ru->get('summary'),
                        ],
                        'slug'       => $slug,
                        'created_at' => $now,
                        'publish_at' => $now,
                        'enabled'    => $en->get('enabled'),
                        'type'       => $type,
                        'entry'      => $entry,
                        'category'   => $category,
                        'author'     => 1,
                        // 'updated_by_id' => 0,
                        'str_id'     => str_random(),
                    ]
                );

                echo "\033[36;5;228mCreated post \033[31;5;228m".$ru->get('title')."\n";
            });

            echo "\033[32;5;228mPosts seeded successfully!\n";
        }
    }

    // /**
    //  * Run the seeder.
    //  */
    // public function run()
    // {
    //     $this->posts->truncate();
    //     $repository = new EntryRepository();

    //     if ($posts = $this->config('posts'))
    //     {
    //         $types = $this->types->all()->each(function ($type) use ($repository)
    //         {
    //             $entryModelName = $this->getEntryModelName($type->getSlug());
    //             $entryModel     = $this->container->make($entryModelName);

    //             $repository->setModel($entryModel)->truncate();
    //         });

    //         foreach ($posts as $key => $post)
    //         {
    //             $categorySlug = isset($post->get('category')) ? $post->get('category') : 'news';
    //             $typeSlug     = isset($post->get('type')) ? $post->get('type') : 'default';
    //             $fields       = isset($post->get('fields')) ? $post->get('fields') : [];

    //             $type     = $this->types->findBySlug($typeSlug);
    //             $category = $this->categories->findBySlug($categorySlug);

    //             $entryModelName = $this->getEntryModelName($typeSlug);
    //             $entryModel     = $this->container->make($entryModelName);

    //             $entry = $entryModel->create($type->getEntryStream()->getAssignments()
    //                 ->mapWithKeys(function ($assignment)
    //             {
    //                     $fieldSlug    = $assignment->getField()->getSlug();
    //                     $fieldContent = ucfirst($fieldSlug).' field content.';

    //                     return [
    //                         $fieldSlug => $fieldContent,
    //                     ];
    //                 })->toArray()
    //             );

    //             $title   = array_get($post, 'title', 'Default title '.$key);
    //             $summary = array_get($post, 'summary', '');

    //             $this->posts->create(array_merge(
    //                 [
    //                     'ru'         => [
    //                         'title'   => $title,
    //                         'summary' => $summary,
    //                     ],
    //                     'slug'       => str_slug($title, '-'),
    //                     'publish_at' => time(),
    //                     'enabled'    => true,
    //                     'type'       => $type,
    //                     'entry'      => $entry,
    //                     'category'   => $category,
    //                     'author'     => 1,
    //                 ],
    //                 array_except(
    //                     $post,
    //                     ['fields', 'title', 'summary', 'slug', 'en']
    //                 )
    //             ));
    //         }

    //         return;
    //     }

    //     $repository->setModel(new PostsDefaultPostsEntryModel());

    //     $repository->truncate();

    //     $welcome = (new PostsDefaultPostsEntryModel())->create(
    //         [
    //             'en' => [
    //                 'content' => '<p>Welcome to PyroCMS!</p>',
    //             ],
    //         ]
    //     );

    //     $this->posts->create(array_except([
    //         'en'         => [
    //             'title'   => 'Welcome to PyroCMS!',
    //             'summary' => 'This is an example post to demonstrate the posts module.',
    //         ],
    //         'slug'       => 'welcome-to-pyrocms',
    //         'publish_at' => time(),
    //         'enabled'    => true,
    //         'type'       => $type,
    //         'entry'      => $welcome,
    //         'category'   => $category,
    //         'author'     => 1,
    //     ], ['fields']));
    // }

    /**
     * Get config value
     *
     * @param  string  $slug The slug
     * @return mixed
     */
    public function config($slug)
    {
        return $this->config->get('defr.module.apex::seeder.'.$slug, []);
    }

    /**
     * Gets the entry model name.
     *
     * @param  string $slug The slug
     * @return string The entry model name.
     */
    public function getEntryModelName($slug)
    {
        $capitalized = ucfirst(camel_case($slug));

        return "Anomaly\Streams\Platform\Model\Posts\Posts{$capitalized}PostsEntryModel";
    }
}
