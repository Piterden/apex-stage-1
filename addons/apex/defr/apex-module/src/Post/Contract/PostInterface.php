<?php namespace Defr\ApexModule\Post\Contract;

/**
 * PostInterface  class
 *
 * @package default
 * @author
 */
interface PostInterface extends \Anomaly\PostsModule\Post\Contract\PostInterface
{

    /**
     * Gets the featured.
     *
     * @return bool The featured.
     */
    public function getFeatured();

    /**
     * Sets the featured.
     *
     * @param  bool   $featured The featured
     * @return bool
     */
    public function setFeatured($featured);

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl();

    /**
     * Gets the related image url.
     *
     * @param  string $fieldName The field name
     * @return string The related image url.
     */
    public function getRelatedImageUrl($fieldName);
}
