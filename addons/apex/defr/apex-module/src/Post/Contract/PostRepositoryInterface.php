<?php namespace Defr\ApexModule\Post\Contract;

/**
 * PostRepositoryInterface  class
 *
 * @package default
 * @author
 */
interface PostRepositoryInterface extends \Anomaly\PostsModule\Post\Contract\PostRepositoryInterface
{

}
