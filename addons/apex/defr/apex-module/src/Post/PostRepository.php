<?php namespace Defr\ApexModule\Post;

/**
 * PostRepository class
 *
 * @package default
 * @author
 */
class PostRepository extends \Anomaly\PostsModule\Post\PostRepository implements \Defr\ApexModule\Post\Contract\PostRepositoryInterface
{

}
