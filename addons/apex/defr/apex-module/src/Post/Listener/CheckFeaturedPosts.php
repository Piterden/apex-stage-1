<?php namespace Defr\ApexModule\Post\Listener;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Contracts\Container\Container;
use Defr\ApexModule\Post\Contract\PostInterface;
use Defr\ApexModule\Post\Contract\PostRepositoryInterface;

/**
 * Class CheckFeaturedPosts
 */
class CheckFeaturedPosts
{

    /**
     * The builder instance.
     *
     * @var Builder
     */
    protected $builder;

    /**
     * The container instance.
     *
     * @var Container
     */
    protected $container;

    /**
     * Posts repository
     *
     * @var PostRepositoryInterface
     */
    protected $posts;

    /**
     * Create new instance of CheckFeaturedPosts class
     *
     * @param Builder                 $builder   The builder
     * @param Container               $container The container
     * @param PostRepositoryInterface $posts     The posts
     */
    public function __construct(
        Builder $builder,
        Container $container,
        PostRepositoryInterface $posts
    )
    {
        $this->container = $container;
        $this->builder   = $builder;
        $this->posts     = $posts;
    }

    /**
     * Handle the command
     */
    public function handle($event)
    {
        $post = $event->getEntry();

        if (!$post instanceof PostInterface)
        {
            return $post;
        }

        $old = (boolean) $post->getOriginal('featured', false);
        $new = (boolean) $post->getAttribute('featured', false);

        if ($new && $old !== $new)
        {
            $featured = $this->posts->getFeatured();
            $count    = $featured->count();

            if ($count > 3)
            {
                while ($featured->count() > 3)
                {
                    $featured->last()->setFeatured(false)->save();
                }
            }
        }

        return $post;
    }
}
