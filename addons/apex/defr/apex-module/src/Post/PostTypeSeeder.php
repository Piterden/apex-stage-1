<?php namespace Defr\ApexModule\Post;

use Illuminate\Config\Repository;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;
use Anomaly\PostsModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;

/**
 * Class TypeSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class PostTypeSeeder extends Seeder
{

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * The type repository.
     *
     * @var TypeRepositoryInterface
     */
    protected $types;

    /**
     * The field repository.
     *
     * @var FieldRepositoryInterface
     */
    protected $fields;

    /**
     * The streams repository.
     *
     * @var StreamRepositoryInterface
     */
    protected $streams;

    /**
     * The assignment repository.
     *
     * @var AssignmentRepositoryInterface
     */
    protected $assignments;

    /**
     * Create a new TypeSeeder instance.
     *
     * @param TypeRepositoryInterface       $types
     * @param FieldRepositoryInterface      $fields
     * @param StreamRepositoryInterface     $streams
     * @param AssignmentRepositoryInterface $assignments
     */
    public function __construct(
        Repository $config,
        TypeRepositoryInterface $types,
        FieldRepositoryInterface $fields,
        StreamRepositoryInterface $streams,
        AssignmentRepositoryInterface $assignments
    )
    {
        $this->types       = $types;
        $this->config      = $config;
        $this->fields      = $fields;
        $this->streams     = $streams;
        $this->assignments = $assignments;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        if ($types = $this->config('types'))
        {
            foreach ($types as $key => $typeData)
            {
                $slug = array_get($typeData, 'slug', 'default_'.$key);

                if ($type = $this->types->findBySlug($slug.'_posts'))
                {
                    $this->types->delete($type);
                }

                /* @var StreamInterface $stream */
                $stream = $this->streams->findBySlugAndNamespace('types', 'posts');

                $locale       = $this->config->get('app.locale');
                $localeFields = $this->getDefaultLocaleFields($stream);

                if ($stream->isTranslatable())
                {
                    $localeData = $this->getLocaleData(
                        $typeData,
                        $key,
                        $localeFields,
                        $locale,
                        $stream
                    );
                }

                $data = array_merge(
                    array_except($typeData, array_merge(
                        array_keys($localeData[$locale]),
                        ['fields']
                    )),
                    $localeData
                );

                $data['theme_layout'] = "theme::layouts/{$slug}.twig";

                dd($data);

                /* @var TypeInterface $type */
                $type   = $this->types->truncate()->create($data);
                $stream = $type->getEntryStream();

                $this->assignments->create(
                    [
                        'translatable' => true,
                        'stream'       => $stream,
                        'field'        => $this->fields->findBySlugAndNamespace('content', 'posts'),
                    ]
                );
            }

            return;
        }

        // if ($type = $this->types->findBySlug('default_posts'))
        // {
        //     $this->types->delete($type);
        // }

        // /* @var TypeInterface $type */
        // $type = $this->types
        // ->truncate()
        // ->create(
        //     [
        //         'en'           => [
        //             'name'        => 'Default',
        //             'description' => 'A simple post type.',
        //         ],
        //         'slug'         => 'default',
        //         'theme_layout' => 'theme::layouts/default.twig',
        //         'layout'       => '{{ post.content.render|raw }}',
        //     ]
        // );

        // $stream = $type->getEntryStream();

        // $this->assignments->create(
        //     [
        //         'translatable' => true,
        //         'stream'       => $stream,
        //         'field'        => $this->fields->findBySlugAndNamespace('content', 'posts'),
        //     ]
        // );
    }

    /**
     * Get seeder config value
     *
     * @param  string       $slug The slug
     * @return mixed|null
     */
    public function config($slug)
    {
        return $this->config->get('defr.module.apex::seeder.'.$slug, null);
    }

    /**
     * Gets the default locale fields.
     *
     * @param  StreamInterface $stream The stream
     * @return array           The default locale fields.
     */
    public function getDefaultLocaleFields(StreamInterface $stream)
    {
        return $stream->translations()->get()->mapWithKeys(
            function ($translation)
            {
                $trans = $translation->toArray();

                return [
                    array_get($trans, 'locale') => array_map(
                        function ($field)
                        {
                            return trans($field);
                        },
                        array_except(
                            $trans,
                            ['id', 'stream_id', 'locale']
                        )
                    ),
                ];
            }
        )->toArray();
    }

    /**
     * Gets the locale data.
     *
     * @param  array           $typeData     The type data
     * @param  integer         $key          The key
     * @param  array           $localeFields The locale fields
     * @param  string          $locale       The locale
     * @param  StreamInterface $stream       The stream
     * @return array           The locale data.
     */
    public function getLocaleData(
        array $typeData,
        int $key,
        array $localeFields,
        string $locale,
        StreamInterface $stream
    )
    {
        return array_merge(
            $localeFields,
            [$locale => array_map(
                function ($field) use ($key, $typeData)
                {
                    $slug = array_reverse(explode('.', $field))[0];

                    return array_get($typeData, $slug, $slug.' '.$key);
                },
                array_except(
                    $stream->translations()->first()->toArray(),
                    ['id', 'stream_id', 'locale']
                )
            )]
        );
    }
}
