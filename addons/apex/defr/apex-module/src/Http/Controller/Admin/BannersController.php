<?php namespace Defr\ApexModule\Http\Controller\Admin;

use Defr\ApexModule\Banner\Form\BannerFormBuilder;
use Defr\ApexModule\Banner\Table\BannerTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class BannersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  BannerTableBuilder $table
     * @return Response
     */
    public function index(BannerTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  BannerFormBuilder $form
     * @return Response
     */
    public function create(BannerFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  BannerFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(BannerFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
