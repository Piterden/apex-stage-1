<?php namespace Defr\ApexModule\Http\Controller\Admin;

use Defr\ApexModule\Recall\Form\RecallFormBuilder;
use Defr\ApexModule\Recall\Table\RecallTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class RecallsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  RecallTableBuilder $table
     * @return Response
     */
    public function index(RecallTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  RecallFormBuilder $form
     * @return Response
     */
    public function create(RecallFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  RecallFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(RecallFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
