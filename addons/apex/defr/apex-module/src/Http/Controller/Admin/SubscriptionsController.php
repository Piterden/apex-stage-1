<?php namespace Defr\ApexModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\ApexModule\Subscription\Form\SubscriptionFormBuilder;
use Defr\ApexModule\Subscription\Table\SubscriptionTableBuilder;

class SubscriptionsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  SubscriptionTableBuilder $table
     * @return Response
     */
    public function index(SubscriptionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  SubscriptionFormBuilder $form
     * @return Response
     */
    public function create(SubscriptionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  SubscriptionFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(SubscriptionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
