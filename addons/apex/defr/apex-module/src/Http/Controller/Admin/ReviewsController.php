<?php namespace Defr\ApexModule\Http\Controller\Admin;

use Defr\ApexModule\Review\Form\ReviewFormBuilder;
use Defr\ApexModule\Review\Table\ReviewTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ReviewsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  ReviewTableBuilder $table
     * @return Response
     */
    public function index(ReviewTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  ReviewFormBuilder $form
     * @return Response
     */
    public function create(ReviewFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  ReviewFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(ReviewFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
