<?php namespace Defr\ApexModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class ApexModule extends Module
{

    /**
     * The module's icon.
     *
     * @var string
     */
    protected $icon = 'cubes';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'banners' => [
            'buttons' => [
                'new_banner',
            ],
        ],
        'reviews' => [
            'buttons' => [
                'new_review',
            ],
        ],
        'recalls',
        'questions',
        'subscriptions',
    ];
}
