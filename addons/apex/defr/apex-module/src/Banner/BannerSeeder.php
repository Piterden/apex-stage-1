<?php namespace Defr\ApexModule\Banner;

use Defr\ApexModule\Banner\Contract\BannerRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Illuminate\Contracts\Config\Repository;

class BannerSeeder extends Seeder
{

    /**
     * The Banner repository.
     *
     * @var BannerRepositoryInterface
     */
    protected $banners;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a new BannerSeeder instance.
     *
     * @param Repository $config
     * @param BannerRepositoryInterface $banners
     */
    public function __construct(
        Repository $config,
        BannerRepositoryInterface $banners
    )
    {
        $this->config = $config;
        $this->banners = $banners;
    }

    /**
     * Run the seeder
     */
    public function run()
    {

    }
}
