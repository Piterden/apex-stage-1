<?php namespace Defr\ApexModule\Banner\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface BannerInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return number The identifier.
     */
    public function getId();

    /**
     * Gets the title.
     *
     * @return string The title.
     */
    public function getTitle();

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription();

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl();

    /**
     * Gets the link title.
     *
     * @return string The link title.
     */
    public function getLinkTitle();

    /**
     * Gets the link url.
     *
     * @return string The link url.
     */
    public function getLinkUrl();
}
