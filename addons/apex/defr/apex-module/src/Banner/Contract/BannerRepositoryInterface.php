<?php namespace Defr\ApexModule\Banner\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BannerRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Makes array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection();
}
