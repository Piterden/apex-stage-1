<?php namespace Defr\ApexModule\Banner;

use Defr\ApexModule\Banner\Contract\BannerInterface;
use Anomaly\Streams\Platform\Model\Apex\ApexBannersEntryModel;

class BannerModel extends ApexBannersEntryModel implements BannerInterface
{

    /**
     * Gets the identifier.
     *
     * @return number The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the title.
     *
     * @return string The title.
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl()
    {
        return $this->image->url();
    }

    /**
     * Gets the link title.
     *
     * @return string The link title.
     */
    public function getLinkTitle()
    {
        return $this->link_title;
    }

    /**
     * Gets the link url.
     *
     * @return string The link url.
     */
    public function getLinkUrl()
    {
        return $this->link_url;
    }
}
