<?php namespace Defr\ApexModule\Banner;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\ApexModule\Banner\Contract\BannerRepositoryInterface;

class BannerRepository extends EntryRepository implements BannerRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BannerModel
     */
    protected $model;

    /**
     * Create a new BannerRepository instance.
     *
     * @param BannerModel $model
     */
    public function __construct(BannerModel $model)
    {
        $this->model = $model;
    }

    /**
     * Makes array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection()
    {
        return $this->model->all()->map(
            /* @var BannerModel $banner */
            function ($banner)
            {
                return [
                    'id'          => $banner->getId(),
                    'title'       => $banner->getTitle(),
                    'description' => $banner->getDescription(),
                    'image'       => $banner->getImageUrl(),
                    'link_title'  => $banner->getLinkTitle(),
                    'link_url'    => $banner->getLinkUrl(),
                ];

            }
        );
    }
}
