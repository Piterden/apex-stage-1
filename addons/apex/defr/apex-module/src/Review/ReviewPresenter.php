<?php namespace Defr\ApexModule\Review;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class ReviewPresenter extends EntryPresenter
{
    public function imageLabel()
    {
        return
        '<span>
            <img width="100" src="'.$this->object->getImageUrl().'" />
        </span>';
    }
}
