<?php namespace Defr\ApexModule\Review;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\ApexModule\Review\Contract\ReviewRepositoryInterface;

class ReviewSeeder extends Seeder
{

    /**
     * The Review repository.
     *
     * @var ReviewRepositoryInterface
     */
    protected $reviews;

    /**
     * Create a new ReviewSeeder instance.
     *
     * @param Repository                $config
     * @param ReviewRepositoryInterface $reviews
     */
    public function __construct(
        ReviewRepositoryInterface $reviews
    )
    {
        $this->reviews = $reviews;
    }

    /**
     * Run the seeder
     */
    public function run()
    {
        $reviewsJson = __DIR__.'/../../resources/seeders/reviews.json';

        echo "\n\033[37;5;228mStarting reviews seeder!\n";

        $this->reviews->truncate();

        echo "\033[35;5;228mReviews cleared!\n";

        if ($data = file_get_contents($reviewsJson))
        {
            foreach (json_decode($data, true) as $key => $review)
            {
                /* @var ReviewInterface $review */
                $review = $this->reviews->create([
                    'sort_order'    => array_get($review, 'sort_order'),
                    'created_at'    => array_get($review, 'created_at'),
                    'created_by_id' => array_get($review, 'created_by_id'),
                    'name'          => array_get($review, 'name'),
                    'image_id'      => array_get($review, 'image_id'),
                    'description'   => array_get($review, 'description'),
                    'profession'    => array_get($review, 'profession'),
                    'link_title'    => array_get($review, 'link_title'),
                    'link_url'      => array_get($review, 'link_url'),
                ]);

                echo "\033[36;5;228mCreated review from \033[31;5;228m"
                .array_get($review, 'name')."\n";
            }

            echo "\033[32;5;228mReviews seeded successfully!\n";
        }
    }
}
