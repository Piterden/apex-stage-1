<?php namespace Defr\ApexModule\Review\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class ReviewTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'image' => [
            'value' => 'entry.image_label',
        ],
        'name',
        'profession',
        'description',
        // 'link_title',
        // 'link_url',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The table assets.
     *
     * @var array
     */
    protected $assets = [
        // 'theme.css' => [
        //     'module::css/table.css',
        // ],
    ];

}
