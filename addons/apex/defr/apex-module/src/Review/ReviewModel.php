<?php namespace Defr\ApexModule\Review;

use Defr\ApexModule\Review\Contract\ReviewInterface;
use Anomaly\Streams\Platform\Model\Apex\ApexReviewsEntryModel;

class ReviewModel extends ApexReviewsEntryModel implements ReviewInterface
{

    /**
     * Get the title.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl()
    {
        $filename   = $this->image->name;
        $foldername = strtolower($this->image->folder->name);
        $diskname   = strtolower($this->image->disk->name);

        return url("app/apex/files-module/{$diskname}/{$foldername}/{$filename}");
    }
}
