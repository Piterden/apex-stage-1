<?php namespace Defr\ApexModule\Review;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\ApexModule\Review\Contract\ReviewRepositoryInterface;

class ReviewRepository extends EntryRepository implements ReviewRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ReviewModel
     */
    protected $model;

    /**
     * Create a new ReviewRepository instance.
     *
     * @param ReviewModel $model
     */
    public function __construct(ReviewModel $model)
    {
        $this->model = $model;
    }

    /**
     * Makes array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection()
    {
        return $this->model->all()
        /* @var ReviewModel $review */
        ->map(function ($review)
        {
            $out = $review->toArray();

            $out['image_url'] = $review->getImageUrl();

            return $out;
        });
    }
}
