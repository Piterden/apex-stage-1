<?php namespace Defr\ApexModule\Review\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface ReviewInterface extends EntryInterface
{

    /**
     * Get the name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl();
}
