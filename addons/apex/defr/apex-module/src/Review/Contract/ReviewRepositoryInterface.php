<?php namespace Defr\ApexModule\Review\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ReviewRepositoryInterface extends EntryRepositoryInterface
{
    /**
     * Makes array for vue-app.
     *
     * @return ReviewCollection
     */
    public function toVueCollection();
}
