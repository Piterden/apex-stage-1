<?php namespace Defr\ApexModule;

// use Defr\ApexModule\Post\PostSeeder;
use Defr\ApexModule\Review\ReviewSeeder;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

class ApexModuleSeeder extends Seeder
{

    /**
     * Run the seeder
     */
    public function run()
    {
        $this->call(ReviewSeeder::class);
        // $this->call(PostSeeder::class);
    }
}
