<?php namespace Defr\ApexModule\Question;

use Defr\ApexModule\Question\Contract\QuestionRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Illuminate\Contracts\Config\Repository;

class QuestionSeeder extends Seeder
{

    /**
     * The Question repository.
     *
     * @var QuestionRepositoryInterface
     */
    protected $questions;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a new QuestionSeeder instance.
     *
     * @param Repository $config
     * @param QuestionRepositoryInterface $questions
     */
    public function __construct(
        Repository $config,
        QuestionRepositoryInterface $questions
    )
    {
        $this->config = $config;
        $this->questions = $questions;
    }

    /**
     * Run the seeder
     */
    public function run()
    {

    }
}
