<?php namespace Defr\ApexModule\Question\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Defr\ApexModule\Question\Contract\QuestionInterface;
use Anomaly\Streams\Platform\Notification\Message\MailMessage;

/**
 * Class NewQuestion
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class NewQuestion extends Notification
{

    use Queueable;

    /**
     * Question which user asked.
     *
     * @var QuestionInterface
     */
    public $question;

    /**
     * Create a new UserHasRegistered instance.
     *
     * @param QuestionInterface $question
     */
    public function __construct(QuestionInterface $question)
    {
        $this->question = $question;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  UserInterface $notifiable
     * @return array
     */
    public function via(UserInterface $notifiable)
    {
        return ['mail'];
    }

    /**
     * Return the mail message.
     *
     * @param  UserInterface $notifiable
     * @return MailMessage
     */
    public function toMail(UserInterface $notifiable)
    {
        $data = $notifiable->toArray();

        return (new MailMessage())
            ->view('anomaly.module.users::notifications.activate_your_account')
            ->subject(trans('anomaly.module.users::notification.activate_your_account.subject', $data))
            ->greeting(trans('anomaly.module.users::notification.activate_your_account.greeting', $data))
            ->line(trans('anomaly.module.users::notification.activate_your_account.instructions', $data))
            ->action(
                trans('anomaly.module.users::notification.activate_your_account.button', $data),
                $notifiable->route('activate', ['question' => $this->question])
            );
    }
}
