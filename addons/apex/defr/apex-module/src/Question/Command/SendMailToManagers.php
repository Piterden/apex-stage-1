<?php namespace Defr\ApexModule\Question\Command;

use Defr\ApexModule\Question\Notification\NewQuestion;
use Defr\ApexModule\Question\Contract\QuestionInterface;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;

/**
 * Class SendMailToManagers
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SendMailToManagers
{

    /**
     * Question data
     *
     * @var QuestionInterface
     */
    protected $question;

    /**
     * Create new SendMailToManagers instance
     *
     * @param QuestionInterface $question The question
     */
    public function __construct(QuestionInterface $question)
    {
        $this->question = $question;
    }

    /**
     * Handle the command
     *
     * @param UserRepositoryInterface $users The users
     * @param RoleRepositoryInterface $roles The roles
     */
    public function handle(
        UserRepositoryInterface $users,
        RoleRepositoryInterface $roles
    )
    {
        if ($role = $roles->findBySlug('question_notifiable'))
        {
            /* @var UserModel $user */
            $role->getUsers()->each(function ($user)
            {
                $user->notify(new NewQuestion($this->recall));
            });
        }
    }
}
