<?php namespace Defr\ApexModule\Question;

use Defr\ApexModule\Question\Contract\QuestionInterface;
use Anomaly\Streams\Platform\Model\Apex\ApexQuestionsEntryModel;

class QuestionModel extends ApexQuestionsEntryModel implements QuestionInterface
{

    /**
     * Gets the identifier.
     *
     * @return int The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the email.
     *
     * @return string The email.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Gets the phone.
     *
     * @return string The phone.
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Gets the city.
     *
     * @return string The city.
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Gets the message.
     *
     * @return string The message.
     */
    public function getMessage()
    {
        return $this->message;
    }
}
