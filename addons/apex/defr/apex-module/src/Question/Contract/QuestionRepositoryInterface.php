<?php namespace Defr\ApexModule\Question\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface QuestionRepositoryInterface extends EntryRepositoryInterface
{

}
