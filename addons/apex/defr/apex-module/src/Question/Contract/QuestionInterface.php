<?php namespace Defr\ApexModule\Question\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface QuestionInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return int The identifier.
     */
    public function getId();

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName();

    /**
     * Gets the email.
     *
     * @return string The email.
     */
    public function getEmail();

    /**
     * Gets the phone.
     *
     * @return string The phone.
     */
    public function getPhone();

    /**
     * Gets the city.
     *
     * @return string The city.
     */
    public function getCity();

    /**
     * Gets the message.
     *
     * @return string The message.
     */
    public function getMessage();
}
