<?php namespace Defr\ApexModule\Question;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Defr\ApexModule\Question\Command\SendMailToManagers;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

class QuestionObserver extends EntryObserver
{

    /**
     * Run after a record is created.
     *
     * @param EntryInterface $entry
     */
    public function created(EntryInterface $entry)
    {
        $this->dispatch(new SendMailToManagers($entry));

        parent::created($entry);
    }
}
