<?php namespace Defr\ApexModule\Recall;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Defr\ApexModule\Recall\Command\SendMailToManagers;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

class RecallObserver extends EntryObserver
{

    /**
     * Run after a record is created.
     *
     * @param EntryInterface $entry
     */
    public function created(EntryInterface $entry)
    {
        $this->commands->dispatch(new SendMailToManagers($entry));

        parent::created($entry);
    }
}
