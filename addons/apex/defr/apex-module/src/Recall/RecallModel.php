<?php namespace Defr\ApexModule\Recall;

use Defr\ApexModule\Recall\Contract\RecallInterface;
use Anomaly\Streams\Platform\Model\Apex\ApexRecallsEntryModel;

class RecallModel extends ApexRecallsEntryModel implements RecallInterface
{

    /**
     * Creating time
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
