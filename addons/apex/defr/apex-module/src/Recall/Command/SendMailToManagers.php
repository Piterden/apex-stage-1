<?php namespace Defr\ApexModule\Recall\Command;

use Defr\ApexModule\Recall\Notification\NewRecall;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;

/**
 * Send email notifications to managers
 *
 * @package defr.module.apex
 * @author
 */
class SendMailToManagers
{

    /**
     * Recall object
     *
     * @var RecallModel
     */
    protected $recall;

    /**
     * Create SendMailToManagers instance
     *
     * @param RecallModel $recall
     */
    public function __construct(EntryInterface $recall)
    {
        $this->recall = $recall;
    }

    /**
     * Handle the command.
     *
     * @return bool
     */
    public function handle(RoleRepositoryInterface $roles)
    {
        if ($role = $roles->findBySlug('recall_notifiable'))
        {
            /* @var UserModel $user */
            $role->getUsers()->each(function ($user)
            {
                $user->notify(new NewRecall($this->recall));
            });
        }
    }
}
