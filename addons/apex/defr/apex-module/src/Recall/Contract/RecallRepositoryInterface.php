<?php namespace Defr\ApexModule\Recall\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface RecallRepositoryInterface extends EntryRepositoryInterface
{

}
