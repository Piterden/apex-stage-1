<?php namespace Defr\ApexModule\Recall\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface RecallInterface extends EntryInterface
{
    public function getCreatedAt();
}
