<?php namespace Defr\ApexModule\Recall;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\ApexModule\Recall\Contract\RecallRepositoryInterface;

class RecallRepository extends EntryRepository implements RecallRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var RecallModel
     */
    protected $model;

    /**
     * Create a new RecallRepository instance.
     *
     * @param RecallModel $model
     */
    public function __construct(RecallModel $model)
    {
        $this->model = $model;
    }
}
