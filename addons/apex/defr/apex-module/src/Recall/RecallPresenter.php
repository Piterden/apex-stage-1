<?php namespace Defr\ApexModule\Recall;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class RecallPresenter extends EntryPresenter
{
    public function createdAt()
    {
        return $this->object->getCreatedAt();
    }
}
