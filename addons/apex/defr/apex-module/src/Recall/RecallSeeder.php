<?php namespace Defr\ApexModule\Recall;

use Defr\ApexModule\Recall\Contract\RecallRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Illuminate\Contracts\Config\Repository;

class RecallSeeder extends Seeder
{

    /**
     * The Recall repository.
     *
     * @var RecallRepositoryInterface
     */
    protected $recalls;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a new RecallSeeder instance.
     *
     * @param Repository $config
     * @param RecallRepositoryInterface $recalls
     */
    public function __construct(
        Repository $config,
        RecallRepositoryInterface $recalls
    )
    {
        $this->config = $config;
        $this->recalls = $recalls;
    }

    /**
     * Run the seeder
     */
    public function run()
    {

    }
}
