<?php namespace Defr\ApexModule\Recall\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Anomaly\Streams\Platform\Notification\Message\MailMessage;

/**
 * Class NewRecall
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class NewRecall extends Notification implements ShouldQueue
{

    use Queueable;

    /**
     * Recall request.
     *
     * @var string
     */
    public $recall;

    /**
     * Create a new NewRecall instance.
     *
     * @param $recall
     */
    public function __construct($recall)
    {
        $this->recall = $recall;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  UserInterface $notifiable
     * @return array
     */
    public function via(UserInterface $notifiable)
    {
        return ['mail'];
    }

    /**
     * Return the mail message.
     *
     * @param  UserInterface $notifiable
     * @return MailMessage
     */
    public function toMail(UserInterface $notifiable)
    {
        $data = $notifiable->toArray();

        return (new MailMessage())
        ->view('defr.module.apex::notifications.recall_request')
        ->subject(trans('defr.module.apex::notification.recall_request.subject', $data))
        ->greeting(trans('defr.module.apex::notification.recall_request.greeting', $data))
        ->line(trans('defr.module.apex::notification.recall_request.instructions', $data));
    }
}
