<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateApexFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'name'        => 'anomaly.field_type.text',
        'phone'       => 'anomaly.field_type.text',
        'city'        => 'anomaly.field_type.text',
        'profession'  => 'anomaly.field_type.text',
        'link_title'  => 'anomaly.field_type.text',
        'link_url'    => 'anomaly.field_type.url',
        'url'         => 'anomaly.field_type.url',
        'email'       => 'anomaly.field_type.email',
        'description' => 'anomaly.field_type.wysiwyg',
        'message'     => 'anomaly.field_type.textarea',
        'image'       => 'anomaly.field_type.file',
        'status'      => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options'       => [
                    'not_processed' => 'Не обработано',
                    'processed'     => 'Обработано',
                ],
                'default_value' => 'not_processed',
            ],
        ],
    ];

}
