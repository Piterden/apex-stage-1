<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateRecallsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'      => 'recalls',
        'trashable' => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'  => ['required' => true],
        'phone' => ['required' => true],
        'status',
    ];

}
