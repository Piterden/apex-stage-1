<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateQuestionsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'questions',
        'title_column' => 'name',
        'trashable'    => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'    => [
            'required' => true,
        ],
        'email'   => [
            'required' => true,
        ],
        'phone'   => [
            'required' => true,
        ],
        'city'    => [
            'required' => true,
        ],
        'message' => [
            'required' => true,
        ],
    ];

}
