<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateSubscriptionsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'subscriptions',
        'title_column' => 'email',
        'trashable'    => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'email' => [
            'required' => true,
        ],

    ];

}
