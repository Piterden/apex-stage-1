<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateBannersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'banners',
        'title_column' => 'title',
        'trashable'    => true,
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title',
        'description',
        'image',
        'link_title',
        'link_url',
    ];

}
