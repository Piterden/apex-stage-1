<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleApexCreateReviewsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'reviews',
        'title_column' => 'name',
        'sortable'     => true,
        'trashable'    => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'        => [
            'required' => true,
        ],
        'image'       => [
            'required' => true,
        ],
        'description' => [
            'required' => true,
        ],
        'profession',
        'link_title',
        'link_url',
    ];

}
