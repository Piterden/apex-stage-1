<?php

return [
    'title'       => 'Апекс',
    'name'        => 'Модуль Апекс',
    'description' => 'Модуль доп. функционала для сайта Апекс',
    'section' => [
        'banners' => 'Баннеры',
        'reviews' => 'Отзывы',
        'recalls' => 'Заявки на звонок',
        'questions' => 'Вопросы',
        'subscriptions' => 'Подписки',
    ]
];
