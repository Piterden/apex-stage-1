<?php

return [
    'title'       => 'Apex',
    'name'        => 'Apex Module',
    'description' => '',
    'section' => [
        'banners' => 'Banners',
        'reviews' => 'Reviews',
        'recalls' => 'Recall Requests',
        'questions' => 'Questions',
        'subscriptions' => 'Subscribes',
    ]
];
