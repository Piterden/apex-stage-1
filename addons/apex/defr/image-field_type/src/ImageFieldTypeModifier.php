<?php namespace Defr\ImageFieldType;

use Anomaly\FilesModule\File\FilePresenter;
use Anomaly\FilesModule\File\Contract\FileInterface;
use Anomaly\FilesModule\File\Contract\FileRepositoryInterface;
use Anomaly\Streams\Platform\Addon\FieldType\FieldTypeModifier;

/**
 * Class ImageFieldTypeModifier
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ImageFieldTypeModifier extends FieldTypeModifier
{

    /**
     * The files repository.
     *
     * @var FileRepositoryInterface
     */
    protected $files;

    /**
     * Create a new ImageFieldTypeModifier instance.
     *
     * @param FileRepositoryInterface $files
     */
    public function __construct(FileRepositoryInterface $files)
    {
        $this->files = $files;
    }

    /**
     * Modify the value for database storage.
     *
     * @param  $value
     * @return int
     */
    public function modify($value)
    {
        if ($value instanceof FilePresenter)
        {
            $value = $value->getObject();
        }

        if ($value instanceof FileInterface)
        {
            return $value->getId();
        }

        if ($value && $file = $this->files->find($value))
        {
            return $file->getId();
        }

        return null;
    }
}
