<?php namespace Defr\ImageFieldType;

use Anomaly\FilesModule\File\FileModel;
use Illuminate\Contracts\Cache\Repository;
use Defr\ImageFieldType\Table\ValueTableBuilder;
use Anomaly\FilesModule\File\Contract\FileInterface;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class ImageFieldType
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ImageFieldType extends FieldType
{

    /**
     * The underlying database column type
     *
     * @var string
     */
    protected $columnType = 'integer';

    /**
     * The input view.
     *
     * @var string
     */
    protected $inputView = 'defr.field_type.image::input';

    /**
     * The field type config.
     *
     * @var array
     */
    protected $config = [
        'folders' => [],
    ];

    /**
     * The cache repository.
     *
     * @var Repository
     */
    protected $cache;

    /**
     * Create a new ImageFieldType instance.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get the relation.
     *
     * @return BelongsTo
     */
    public function getRelation()
    {
        $entry = $this->getEntry();

        return $entry->belongsTo(
            array_get($this->config, 'related', FileModel::class),
            $this->getColumnName()
        );
    }

    /**
     * Get the config.
     *
     * @return array
     */
    public function getConfig()
    {
        $config = parent::getConfig();

        $post = str_replace('M', '', ini_get('post_max_size'));
        $file = str_replace('M', '', ini_get('upload_max_filesize'));

        $server = $file > $post ? $post : $file;

        $max = array_get($config, 'max');

        if ($max && $max > $server)
        {
            $max = $server;
        }

        $width  = $this->config('width', 0);
        $height = $this->config('height', 0);

        $presenter = $this->getPresenter();

        array_set($config, 'max', $max);
        array_set($config, 'width', (integer) $width);
        array_set($config, 'height', (integer) $height);
        array_set($config, 'folders', (array) $this->config('folders', []));
        array_set($config, 'size_label',
            $presenter->label($width.'px X '.$height.'px', 'warning')
        );

        return $config;
    }

    /**
     * Get the database column name.
     *
     * @return null|string
     */
    public function getColumnName()
    {
        return parent::getColumnName().'_id';
    }

    /**
     * Return the config key.
     *
     * @return string
     */
    public function configKey()
    {
        $key = md5(json_encode($this->getConfig()));

        $this->cache->put('image-field_type::'.$key, $this->getConfig(), 30);

        return $key;
    }

    /**
     * Value table.
     *
     * @return string
     */
    public function valueTable()
    {
        $table = app(ValueTableBuilder::class);

        $file = $this->getValue();

        if ($file instanceof FileInterface)
        {
            $file = $file->getId();
        }

        return $table->setUploaded([$file])->build()->load()->getTableContent();
    }
}
