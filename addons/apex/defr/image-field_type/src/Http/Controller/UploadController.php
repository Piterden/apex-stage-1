<?php namespace Defr\ImageFieldType\Http\Controller;

use Anomaly\FilesModule\File\FileUploader;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\ImageFieldType\Table\ImageTableBuilder;
use Anomaly\FilesModule\Folder\Command\GetFolder;
use Defr\ImageFieldType\Table\UploadTableBuilder;
use Defr\ImageFieldType\Command\ValidateDimensions;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;

/**
 * Class UploadController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class UploadController extends AdminController
{

    use DispatchesJobs;

    /**
     * Return the uploader.
     *
     * @param  UploadTableBuilder      $table
     * @param  Repository              $cache   The cache
     * @param  number                  $folder  The folder
     * @param  string                  $key     The key
     * @return \Illuminate\View\View
     */
    public function index(
        UploadTableBuilder $table,
        Repository $cache,
        $folder,
        $key
    )
    {
        return $this->view->make(
            'defr.field_type.image::upload/index',
            [
                'key'    => $key,
                'table'  => $table->make()->getTable(),
                'folder' => $this->dispatch(new GetFolder($folder)),
                'config' => $cache->get('image-field_type::'.$key, []),
            ]
        );
    }

    /**
     * Upload a file.
     *
     * @param  FolderRepositoryInterface $folders
     * @param  FileUploader              $uploader
     * @param  Repository                $cache      The cache
     * @param  string                    $key        The key
     * @return Response
     */
    public function upload(
        FolderRepositoryInterface $folders,
        FileUploader $uploader,
        Repository $cache,
        $key
    )
    {
        if ($file = $uploader->upload(
            $this->request->file('upload'),
            $folders->find($this->request->get('folder'))
        ))
        {
            if ($error = $this->dispatch(new ValidateDimensions($key, $file)))
            {
                return $this->response->json([
                    'error' => $error,
                ], 500);
            }

            return $this->response->json();
        }

        return $this->response->json([
            'error' => 'There was a problem uploading the file.',
        ], 500);
    }

    /**
     * Return the recently uploaded files.
     *
     * @param  ImageTableBuilder                            $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function recent(UploadTableBuilder $table)
    {
        return $table->setUploaded(explode(',', $this->request->get('uploaded')))
        ->make()
        ->getTableContent();
    }
}
