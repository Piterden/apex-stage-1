<?php namespace Defr\ImageFieldType\Http\Controller;

use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Repository;
use Defr\ImageFieldType\Table\ImageTableBuilder;
use Defr\ImageFieldType\Table\ValueTableBuilder;
use Anomaly\FilesModule\Folder\Command\GetFolder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;

/**
 * Class ImagesController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ImagesController extends AdminController
{

    /**
     * Return an index of existing files.
     *
     * @param  ImageTableBuilder                            $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ImageTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Return a list of folders to choose from.
     *
     * @param  FolderRepositoryInterface $folders
     * @param  Repository                $cache
     * @param  Request                   $request
     * @return \Illuminate\View\View
     */
    public function choose(
        FolderRepositoryInterface $folders,
        Repository $cache,
        Request $request
    )
    {
        $allowed = [];
        $key     = $request->route('key');

        $config = $cache->get('image-field_type::'.$key, []);

        foreach (array_get($config, 'folders', []) as $identifier)
        {
            /* @var FolderInterface $folder */
            if ($folder = $this->dispatch(new GetFolder($identifier)))
            {
                $allowed[] = $folder;
            }
        }

        if (!$allowed)
        {
            $allowed = $folders->all();
        }

        return $this->view->make(
            'defr.field_type.image::choose',
            [
                'folders' => $allowed,
                'key'     => $key,
            ]
        );
    }

    /**
     * Return a table of selected files.
     *
     * @param  ValueTableBuilder $table
     * @return null|string
     */
    public function selected(ValueTableBuilder $table)
    {
        return $table->setUploaded(explode(
            ',',
            $this->request->get('uploaded')
        ))->make()->getTableContent();
    }
}
