<?php namespace Defr\ImageFieldType\Command;

use Illuminate\Http\Request;
use Defr\ImageFieldType\ImageFieldType;

/**
 * Class GetUploadImage
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetUploadImage
{

    /**
     * The field type instance.
     *
     * @var ImageFieldType
     */
    protected $fieldType;

    /**
     * Create a new GetUploadImage instance.
     *
     * @param ImageFieldType $fieldType
     */
    public function __construct(ImageFieldType $fieldType)
    {
        $this->fieldType = $fieldType;
    }

    /**
     * Handle the command.
     *
     * @param  Request                                                     $request
     * @return array|\Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public function handle(Request $request)
    {
        return $request->file($this->fieldType->getInputName());
    }
}
