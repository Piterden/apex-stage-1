<?php namespace Defr\ImageFieldType\Command;

use Illuminate\Contracts\Cache\Repository;
use Anomaly\FilesModule\File\Contract\FileInterface;

/**
 * Class for validate dimensions.
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ValidateDimensions
{

    /**
     * Cache key
     *
     * @var string
     */
    protected $key;

    /**
     * File model
     *
     * @var FileInterface
     */
    protected $file;

    /**
     * Create ValidateDimensions instance
     *
     * @param string        $key  The key
     * @param FileInterface $file The file
     */
    public function __construct(string $key, FileInterface $file)
    {
        $this->key  = $key;
        $this->file = $file;
    }

    /**
     * Handle the command
     *
     * @param  Repository $cache The cache
     * @return array
     */
    public function handle(Repository $cache)
    {
        $config = $cache->get('image-field_type::'.$this->key, []);

        $attributes = $this->file->getAttributes();

        $configHeight = array_get($config, 'height', 0);
        $actualHeight = array_get($attributes, 'height', 0);

        if ($configHeight > 0 && $configHeight !== $actualHeight)
        {
            return "The height of image should be {$configHeight} px.";
        }

        // $configWidth  = array_get($config, 'width', 0);
        // $actualWidth  = array_get($attributes, 'width', 0);
        //
        // if ($configWidth > 0 && $configWidth !== $actualWidth)
        // {
        //     return "The width of image should be {$configWidth} px.";
        // }

        return false;
    }
}
