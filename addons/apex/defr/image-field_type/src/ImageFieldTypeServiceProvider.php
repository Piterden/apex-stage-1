<?php namespace Defr\ImageFieldType;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

/**
 * Class ImageFieldTypeServiceProvider
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ImageFieldTypeServiceProvider extends AddonServiceProvider
{

    /**
     * The singleton bindings.
     *
     * @var array
     */
    protected $singletons = [
        'Defr\ImageFieldType\FileFieldTypeModifier' => 'Defr\ImageFieldType\FileFieldTypeModifier',
    ];

    /**
     * The addon routes.
     *
     * @var array
     */
    protected $routes = [
        'streams/image-field_type/index/{key}'           => 'Defr\ImageFieldType\Http\Controller\ImagesController@index',
        'streams/image-field_type/choose/{key}'          => 'Defr\ImageFieldType\Http\Controller\ImagesController@choose',
        'streams/image-field_type/selected'              => 'Defr\ImageFieldType\Http\Controller\ImagesController@selected',
        'streams/image-field_type/upload/{folder}/{key}' => 'Defr\ImageFieldType\Http\Controller\UploadController@index',
        'streams/image-field_type/handle/{key}'          => 'Defr\ImageFieldType\Http\Controller\UploadController@upload',
        'streams/image-field_type/recent'                => 'Defr\ImageFieldType\Http\Controller\UploadController@recent',
    ];

}
