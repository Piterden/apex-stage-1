VueCrop.setOptions({
  setSelect: [25, 25, 250, 250],
  aspectRatio: 1,
  bgColor: '#2C7152'
});

new Vue({
  el: 'body',
  data: function() {
    return {
      src: '',
      width: 0,
      height: 0,
      coords: ''
    }
  },
  methods: {
    test: function(event, selection, coordinates) {
      this.coords = coordinates;
    },
    setProps: function(id, width, height) {
      console.log(id, width, height);
    }
  }
}).$mount();
