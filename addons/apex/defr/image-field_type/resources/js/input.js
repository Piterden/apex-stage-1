/*eslint no-new: 0*/
/*eslint no-undef: 0*/
/*eslint space-after-keywords: 0*/
/*eslint keyword-spacing: 0*/
/*eslint space-return-throw-case: 0*/
/*eslint semi: [1, "always"]*/
$(document)
  .on('ajaxComplete ready', function() {
    // Initialize file pickers
    $('[data-provides="defr.field_type.image"]')
      .each(function() {
        var input = $(this);
        var field = input.data('field_name');
        var wrapper = input.closest('.form-group');
        var modal = $('#' + field + '-modal');

        modal.on('click', '[data-file]', function(e) {
          e.preventDefault();

          var $this = $(this);
          var row = $this.closest('tr');
          var label = row.find('.label.label-info.label-sm');
          var labelText = label.text();

          row.siblings().removeClass('table-danger');

          if ($this.data('width') !== input.data('width') ||
            $this.data('height') !== input.data('height')) {
            row.addClass('table-danger');

            if (!labelText.endsWith('size')) {
              label.text(labelText + ' is not allowed size');
            }

            return false;
          }

          modal.find('.modal-content').append(
            '<div class="modal-loading"><div class="active loader"></div></div>'
          );

          wrapper.find('.selected').load(
            REQUEST_ROOT_PATH + '/streams/image-field_type/selected?uploaded=' +
              $this.data('file'),
            function() {
              modal.modal('hide');
            }
          );

          input.val($this.data('file'));
        });

        $(wrapper)
          .on('click', '[data-dismiss="modal"]', function(e) {
            e.preventDefault();

            input.val('');

            wrapper.find('.selected')
              .load(REQUEST_ROOT_PATH + '/streams/image-field_type/selected');
          });
      });
  });

