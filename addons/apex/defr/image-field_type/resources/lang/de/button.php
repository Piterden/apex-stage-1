<?php

return [
    'select_image' => 'Datei auswählen',
    'remove'      => 'Entfernen',
    'change'      => 'Ändern',
];
