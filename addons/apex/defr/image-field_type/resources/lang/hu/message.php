<?php

return [
    'choose_image'      => 'Melyik fájlt szeretnéd használni?',
    'choose_folder'    => 'Melyik könyvtárba szeretnél feltölteni?',
    'upload'           => 'Kattint ide, vagy ejtsd ide a fájlokat a feltöltéshez.',
    'no_image_selected' => 'Nincs fájl kiválasztva.',
    'no_uploads'       => 'Nincs fájl feltöltve.',
    'overwrite'        => 'már feltöltve. Szeretnéd felülírni?',
    'uploading'        => 'Feltöltés',
    'loading'          => 'Betöltés',
];
