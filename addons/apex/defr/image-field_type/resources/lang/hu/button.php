<?php

return [
    'select_image' => 'Válassz Fájlt',
    'upload'      => 'Feltöltés',
    'remove'      => 'Eltávolítás',
    'change'      => 'Változtatás',
];
