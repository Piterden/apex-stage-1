<?php

return [
    'select_image' => '選擇檔案',
    'upload' => '上傳',
    'remove' => '移除',
    'change' => '更改',
];
