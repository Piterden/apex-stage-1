<?php

return [
    'select_image' => 'Choisir un fichier',
    'remove'      => 'Supprimer',
    'change'      => 'Modifier',
];
