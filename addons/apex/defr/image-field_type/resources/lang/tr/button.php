<?php

return [
    'select_image' => 'Dosya Seç',
    'upload'      => 'Yükle',
    'remove'      => 'Kaldır',
    'change'      => 'Değiştir',
];
