<?php

return [
    'select_image' => 'اختر ملف',
    'upload'      => 'رفع',
    'remove'      => 'حذف',
    'change'      => 'تغير',
];
