<?php

return [
    'title'       => 'Image',
    'name'        => 'Image Field Type',
    'description' => 'A image upload field type.',
];
