<?php

return [
    'folders' => [
        'name'         => 'Folders',
        'instructions' => 'Specify which folders are available for this field. Leave blank to display all folders.',
        'warning'      => 'Existing folder permissions take precedence over selected folders.',
    ],
    'max'     => [
        'name'         => 'Max Upload Size',
        'instructions' => 'Specify the max upload size in <strong>megabytes</strong>.',
        'warning'      => 'If not specified the folder max and then server max will be used instead.',
    ],
    'width'     => [
        'name'         => 'Image Width',
        'instructions' => 'Specify the width of the image in <strong>pixels</strong>.',
        // 'warning'      => 'If not specified the folder max and then server max will be used instead.',
    ],
    'height'     => [
        'name'         => 'Image Height',
        'instructions' => 'Specify the height of the image in <strong>pixels</strong>.',
        // 'warning'      => 'If not specified the folder max and then server max will be used instead.',
    ],
];
