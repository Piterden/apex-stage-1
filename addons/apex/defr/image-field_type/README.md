# File Field Type

*defr.field_type.image*

#### A file upload field type.

The file file type provides a file input that uploads to the Files module.
