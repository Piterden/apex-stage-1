<?php

use Defr\CatalogModule\ProductType\ProductTypeModel;
use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrExtensionProdTypeLinkTypeCreateProdTypeLinkTypeFields
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DefrExtensionProdTypeLinkTypeCreateProdTypeLinkTypeFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'description' => 'anomaly.field_type.textarea',
        'type'        => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => ProductTypeModel::class,
            ],
        ],
    ];
}
