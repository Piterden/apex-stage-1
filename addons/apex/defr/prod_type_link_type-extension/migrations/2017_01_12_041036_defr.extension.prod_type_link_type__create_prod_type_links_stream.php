<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrExtensionProdTypeLinkTypeCreateProdTypeLinksStream
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DefrExtensionProdTypeLinkTypeCreateProdTypeLinksStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'prod_type_links',
        'title_column' => 'title',
        'translatable' => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title'       => [
            'translatable' => true,
        ],
        'description' => [
            'translatable' => true,
        ],
        'type'        => [
            'required' => true,
        ],
    ];

}
