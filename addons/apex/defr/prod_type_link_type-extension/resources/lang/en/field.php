<?php

return [
    'title'       => [
        'name'         => 'Title',
        'instructions' => 'Enter the link title.',
        'placeholder'  => 'About Us',
    ],
    'type'         => [
        'name'         => 'Product Type',
    ],
    'description' => [
        'name'         => 'Description',
        'instructions' => 'The description can be used for extra link text.',
    ],
];
