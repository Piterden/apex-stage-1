<?php namespace Defr\ProdTypeLinkTypeExtension;

use Anomaly\NavigationModule\Link\Contract\LinkInterface;
use Anomaly\NavigationModule\Link\Type\LinkTypeExtension;
use Defr\ProdTypeLinkTypeExtension\Form\ProdTypeLinkTypeFormBuilder;

class ProdTypeLinkTypeExtension extends LinkTypeExtension
{

    /**
     * This extension provides...
     *
     * @var null|string
     */
    protected $provides = 'anomaly.module.navigation::link_type.product_type';

    /**
     * Return the product type URL.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function url(LinkInterface $link)
    {
        /* @var ProdTypeLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $relationship */
        if (!$type = $entry->getType())
        {
            return url('');
        }

        return url($type->getPath());
    }

    /**
     * Return the entry title.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function title(LinkInterface $link)
    {
        /* @var ProdTypeLinkTypeModel $entry */
        $entry = $link->getEntry();

        if ($entry->getTitle())
        {
            return $entry->getTitle();
        }

        /* @var EntryInterface $product_type */
        if (!$type = $entry->getType())
        {
            return '[Broken Link]';
        }

        return $type->getTitle();
    }

    /**
     * Return if the link exists or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function exists(LinkInterface $link)
    {
        /* @var ProdTypeLinkTypeModel $entry */
        $entry = $link->getEntry();

        return (bool) $entry->getType();
    }

    /**
     * Return if the link is enabled or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function enabled(LinkInterface $link)
    {
        /* @var ProdTypeLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var ProdTypeInterface $type */
        if ($type = $entry->getType())
        {
            return $type->isEnabled();
        }

        return false;
    }

    /**
     * Return the form builder for the link type entry.
     *
     * @return FormBuilder
     */
    public function builder()
    {
        return app(ProdTypeLinkTypeFormBuilder::class);
    }

}
