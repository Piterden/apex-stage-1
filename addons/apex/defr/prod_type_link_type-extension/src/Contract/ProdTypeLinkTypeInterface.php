<?php namespace Defr\ProdTypeLinkTypeExtension\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface ProdTypeLinkTypeInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ProdTypeLinkTypeInterface extends EntryInterface
{

    /**
     * Get the product type.
     *
     * @return ProdTypeInterface
     */
    public function getType();
}
