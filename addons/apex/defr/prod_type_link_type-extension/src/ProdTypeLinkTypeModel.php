<?php namespace Defr\ProdTypeLinkTypeExtension;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\ProdTypeLinkTypeExtension\Contract\ProdTypeLinkTypeInterface;
use Anomaly\Streams\Platform\Model\ProdTypeLinkType\ProdTypeLinkTypeProdTypeLinksEntryModel;

/**
 * Class ProdTypeLinkTypeModel
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProdTypeLinkTypeModel extends ProdTypeLinkTypeProdTypeLinksEntryModel implements ProdTypeLinkTypeInterface
{

    /**
     * Eager load these relations.
     *
     * @var array
     */
    protected $with = [
        'translations',
    ];

    /**
     * Get the product type.
     *
     * @return ProdTypeInterface
     */
    public function getType()
    {
        return $this->type;
    }
}
