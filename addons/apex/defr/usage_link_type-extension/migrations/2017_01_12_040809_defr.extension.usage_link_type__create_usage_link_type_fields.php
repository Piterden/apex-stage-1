<?php

use Defr\CatalogModule\ProductUsage\ProductUsageModel;
use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrExtensionUsageLinkTypeCreateUsageLinkTypeFields
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DefrExtensionUsageLinkTypeCreateUsageLinkTypeFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'description' => 'anomaly.field_type.textarea',
        'usage'       => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => ProductUsageModel::class,
            ],
        ],
    ];
}
