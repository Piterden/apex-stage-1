<?php namespace Defr\UsageLinkTypeExtension;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\UsageLinkTypeExtension\Contract\UsageLinkTypeInterface;
use Anomaly\Streams\Platform\Model\UsageLinkType\UsageLinkTypeUsageLinksEntryModel;

/**
 * Class UsageLinkTypeModel
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class UsageLinkTypeModel extends UsageLinkTypeUsageLinksEntryModel implements UsageLinkTypeInterface
{

    /**
     * Eager load these relations.
     *
     * @var array
     */
    protected $with = [
        'translations',
    ];

    /**
     * Get the usage.
     *
     * @return UsageInterface
     */
    public function getUsage()
    {
        return $this->usage;
    }
}
