<?php namespace Defr\UsageLinkTypeExtension;

use Anomaly\NavigationModule\Link\Contract\LinkInterface;
use Anomaly\NavigationModule\Link\Type\LinkTypeExtension;
use Defr\UsageLinkTypeExtension\Form\UsageLinkTypeFormBuilder;

class UsageLinkTypeExtension extends LinkTypeExtension
{

    /**
     * This extension provides...
     *
     * @var null|string
     */
    protected $provides = 'anomaly.module.navigation::link_type.usage';

    /**
     * Return the usage URL.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function url(LinkInterface $link)
    {
        /* @var UsageLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $relationship */
        if (!$usage = $entry->getUsage())
        {
            return url('');
        }

        return url($usage->getPath());
    }

    /**
     * Return the entry title.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function title(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $usage */
        if (!$usage = $entry->getUsage())
        {
            return '[Broken Link]';
        }

        return $usage->getTitle();
    }

    /**
     * Return if the link exists or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function exists(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        return (bool) $entry->getUsage();
    }

    /**
     * Return if the link is enabled or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function enabled(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $usage */
        if ($usage = $entry->getUsage())
        {
            return $usage->isEnabled();
        }

        return false;
    }

    /**
     * Return the form builder for the link type entry.
     *
     * @return FormBuilder
     */
    public function builder()
    {
        return app(UsageLinkTypeFormBuilder::class);
    }

}
