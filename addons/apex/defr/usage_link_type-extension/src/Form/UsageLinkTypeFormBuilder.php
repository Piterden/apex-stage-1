<?php namespace Defr\UsageLinkTypeExtension\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class UsageLinkTypeFormBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class UsageLinkTypeFormBuilder extends FormBuilder
{

}
