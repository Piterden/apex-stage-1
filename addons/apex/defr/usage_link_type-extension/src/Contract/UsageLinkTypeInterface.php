<?php namespace Defr\UsageLinkTypeExtension\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface UsageLinkTypeInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface UsageLinkTypeInterface extends EntryInterface
{

    /**
     * Get the related usage.
     *
     * @return EntryInterface
     */
    public function getUsage();
}
