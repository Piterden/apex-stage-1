<?php

return [
    'products'   => [
        'read',
        'write',
        'delete',
    ],
    'types'      => [
        'read',
        'write',
        'delete',
    ],
    'categories' => [
        'read',
        'write',
        'delete',
    ],
    'brands'     => [
        'read',
        'write',
        'delete',
    ],
    'variants'     => [
        'read',
        'write',
        'delete',
    ],
    'properties' => [
        'read',
        'write',
        'delete',
    ],
    'fields' => [
        'manage',
    ],
];
