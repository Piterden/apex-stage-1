<?php

use Roumen\Sitemap\Sitemap;
use Illuminate\Contracts\Config\Repository;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

return [
    'lastmod' => function (ProductRepositoryInterface $products)
    {

        if (!$product = $products->lastModified())
        {
            return null;
        }

        return $product->lastModified()->toAtomString();
    },
    'entries' => function (ProductRepositoryInterface $products)
    {

        /* @var \Anomaly\ProductsModule\Product\ProductCollection $products */
        $products = $products->all();

        return $products->live();
    },
    'handler' => function (Sitemap $sitemap, Repository $config, ProductInterface $entry)
    {

        $translations = [];

        foreach ($config->get('streams::locales.enabled') as $locale)
        {
            if ($locale != $config->get('streams::locales.default'))
            {
                $translations[] = [
                    'language' => $locale,
                    'url'      => url($locale.$entry->path()),
                ];
            }
        }

        $sitemap->add(
            url($entry->path()),
            $entry->lastModified()->toAtomString(),
            0.5,
            'monthly',
            [],
            null,
            $translations
        );
    },
];
