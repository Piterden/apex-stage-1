<?php

return [
    'choose_type'       => 'Выберите тип товара.',
    'choose_field_type' => 'Выберите тип характеристики.',
    'choose_field'      => 'Выберите характеристику.',
    'tagged'            => 'С тегом &amp;quot;:tag&amp;quot;',
    'scheduled'         => 'В очереди',
    'status'            => 'Статус',
    'live'              => 'Онлайн',
    'draft'             => 'Черновик',
];
