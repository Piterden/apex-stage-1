<?php

return [
    'organization' => 'Организация',
    'permalinks'   => 'Ссылки',
    'advanced'     => 'Продвинутые',
    'general'      => 'Основные',
    'display'      => 'Отображать',
    'options'      => 'Опции',
    'fields'       => 'Поля',
    'layout'       => 'Расположение',
    'product'      => 'Товар',
    'variants'     => 'Вариации',
    'seo'          => 'СЕО',
    'main'         => 'Основное',
    'media'        => 'Медиа',
    'fields'       => 'Доп. поля',
    'options'      => 'Настройки',
    'properties'   => 'Физические свойства',
];
