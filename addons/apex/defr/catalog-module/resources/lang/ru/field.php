<?php

return [
    'name'                 => [
        'label'        => 'Название',
        'name'         => 'Название',
        'instructions' => [
            'types'          => 'Specify a short descriptive name for this product type.',
            'product_usages' => 'Specify a short descriptive name for this category.',
        ],
    ],
    'title'                => [
        'name' => 'Заголовок',
    ],
    'slug'                 => [
        'name'         => 'Псевдоним',
        'instructions' => [
            'types'          => 'The slug is used in making the database table for products of this type.',
            'product_usages' => 'The slug is used in building the category&amp;#039;s URL.',
            'products'       => 'The slug is used in building the product&amp;#039;s URL.',
        ],
    ],
    'image'                => [
        'name' => 'Изображение',
        // 'instructions' => 'AAAAAA',
    ],
    'brand_logo'           => [
        'name'    => 'Логотип бренда',
        'warning' => 'Размер 250х100 px.',
    ],
    'button_text'          => [
        'name' => 'Текст кнопки',
    ],
    'image_label'          => [
        'name' => 'Превью',
    ],
    'preview'              => [
        'name'         => 'Изображение для отображения в карточках',
        'instructions' => 'Если не указано, то будет показана уменьшеная главная картинка.',
    ],
    'product'              => [
        'name' => 'Товар',
    ],
    'description'          => [
        'name'         => 'Описание',
        'instructions' => [
            'types'          => 'Briefly describe the product type.',
            'product_usages' => 'Briefly describe the category.',
        ],
        'warning'      => 'This may or may not be displayed publicly depending on how your website was built.',
    ],
    'tags'                 => [
        'name'         => 'Теги',
        'instructions' => 'Specify any organizational tags to help group your product with others.',
    ],
    'meta_title'           => [
        'name'         => 'Meta заголовок',
        'instructions' => 'Specify the SEO title.',
        'warning'      => 'The product title will be used by default.',
    ],
    'meta_description'     => [
        'name'         => 'Meta описание',
        'instructions' => 'Specify the SEO description.',
    ],
    'meta_keywords'        => [
        'name'         => 'Meta ключевые слова',
        'instructions' => 'Specify the SEO keywords.',
    ],
    'publish_at'           => [
        'name'         => 'Дата публикации',
        'instructions' => 'Specify start time for this station.',
        'warning'      => 'If set to the future, this stations will not played until then.',
    ],
    'unpublish_at'         => [
        'name'         => 'Дата снятия с публикации',
        'instructions' => 'Specify when station will stop broadcast.',
        'warning'      => 'When this stations will stop playing.',
    ],
    'status_label'         => [
        'name'   => 'Статус',
        'option' => [
            'hide'      => 'Скрывать в фильтрах',
            'show'      => 'Отображать в фильтрах',
            'live'      => 'Опубликован',
            'draft'     => 'Черновик',
            'dead'      => 'Снят с публикации',
            'wait'      => 'В ожиждании публикации',
            'scheduled' => 'В очереди   ',
        ],
    ],
    'status'               => [
        'name'   => 'Статус',
        'option' => [
            'hide'      => 'Скрывать в фильтрах',
            'show'      => 'Отображать в фильтрах',
            'live'      => 'Опубликован',
            'draft'     => 'Черновик',
            'dead'      => 'Снят с публикации',
            'wait'      => 'В ожиждании публикации',
            'scheduled' => 'В очереди   ',
        ],
    ],
    'product_usage'        => [
        'name' => 'Область применения',
    ],
    'product_usages'       => [
        'name' => 'Области применения',
    ],
    'product_usages_label' => [
        'name' => 'Области применения',
    ],
    'brand'                => [
        'name' => 'Бренд',
    ],
    'type'                 => [
        'name' => 'Тип товара',
    ],
    'content'              => [
        'name' => 'Контент',
    ],
    'sku'                  => [
        'name' => 'Артикул',
    ],
    'summary'              => [
        'name' => 'Описание',
    ],
    'enabled'              => [
        'name' => 'Опубликован',
    ],
    'featured'             => [
        'name' => 'Хит',
    ],
    'author'               => [
        'name' => 'Автор',
    ],
    'color'                => [
        'name' => 'Цвет',
    ],
    'colors'               => [
        'name' => 'Цвета',
    ],
    'new'                  => [
        'name' => 'Новинка',
    ],
    'capacity'             => [
        'name' => 'Емкость',
    ],
    'advantages'           => [
        'name' => 'Приемущества',
    ],
    'for_whom'             => [
        'name' => 'Для кого',
    ],
    'advices'              => [
        'name' => 'Советы',
    ],
    'attentions'           => [
        'name' => 'Предупреждения',
    ],
    'theme_layout'         => [
        'name' => 'Готовый шаблон',
    ],
    'layout'               => [
        'name' => 'Шаблон',
    ],
    'video'                => [
        'name' => 'Видео',
    ],
    'show'                 => [
        'name' => 'Отображать',
    ],
    'assignments_count'    => [
        'name' => 'Кол-во связей',
    ],
    'value'                => [
        'name' => 'Значение',
    ],
    'price'                => [
        'name' => 'Цена',
    ],
];
