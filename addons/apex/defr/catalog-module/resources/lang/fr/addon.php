<?php

return [
    'title'       => 'Articles',
    'name'        => 'Module Articles',
    'description' => 'Module polyvalent pour la gestion d\'articles, de billets, etc...',
    'section'     => [
        'products'      => 'Articles',
        'types'      => 'ProductTypes',
        'fields'     => 'Champs',
        'categories' => 'Catégories',
    ],
];
