<?php

return [
    'new_product'     => 'Nouvel article',
    'new_category' => 'Nouvelle catégorie',
    'new_type'     => 'Nouveau type',
    'new_field'    => 'Nouveau champs',
    'add_field'    => 'Ajouter champs',
];
