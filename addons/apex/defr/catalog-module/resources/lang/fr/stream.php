<?php

return [
    'products'      => [
        'name' => 'Articles',
    ],
    'product_types' => [
        'name' => 'ProductTypes d\'article',
    ],
    'categories' => [
        'name' => 'Catégories',
    ],
];
