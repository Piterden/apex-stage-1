<?php

return [
    'new_product'     => 'Új Hír',
    'new_category' => 'Új Kategória',
    'new_type'     => 'Új Típus',
    'new_field'    => 'Új Mező',
    'add_field'    => 'Mező Hozzáadása',
];
