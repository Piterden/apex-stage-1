<?php

return [
    'organization' => 'Szervezet',
    'permalinks'   => 'Permalinkek',
    'advanced'     => 'Haladó',
    'general'      => 'Általános',
    'display'      => 'Megjelenés',
    'options'      => 'Opciók',
    'fields'       => 'Mezők',
    'layout'       => 'Elrendezés',
    'product'         => 'Hír',
    'seo'          => 'SEO',
];
