<?php

return [
    'name'             => [
        'name'         => 'Name',
        'instructions' => [
            'types'      => 'Specify a short descriptive name for this product type.',
            'categories' => 'Specify a short descriptive name for this category.',
        ],
    ],
    'slug'             => [
        'name'         => 'Slug',
        'instructions' => [
            'types'      => 'The slug is used in making the database table for products of this type.',
            'categories' => 'The slug is used in building the category\'s URL.',
            'products'   => 'The slug is used in building the product\'s URL.',
        ],
    ],
    'image'            => [
        'name' => 'Image',
    ],
    'user'             => [
        'name' => 'User',
    ],
    'product'          => [
        'name' => 'Product',
    ],
    'description'      => [
        'name'         => 'Description',
        'instructions' => [
            'types'      => 'Briefly describe the product type.',
            'categories' => 'Briefly describe the category.',
        ],
        'warning'      => 'This may or may not be displayed publicly depending on how your website was built.',
    ],
    'tags'             => [
        'name'         => 'Tags',
        'instructions' => 'Specify any organizational tags to help group your product with others.',
    ],
    'meta_title'       => [
        'name'         => 'Meta Title',
        'instructions' => 'Specify the SEO title.',
        'warning'      => 'The product title will be used by default.',
    ],
    'meta_description' => [
        'name'         => 'Meta Description',
        'instructions' => 'Specify the SEO description.',
    ],
    'meta_keywords'    => [
        'name'         => 'Meta Keywords',
        'instructions' => 'Specify the SEO keywords.',
    ],
    'publish_at'       => [
        'name'         => 'Start at',
        'instructions' => 'Specify start time for this station.',
        'warning'      => 'If set to the future, this stations will not played until then.',
    ],
    'unpublish_at'     => [
        'name'         => 'Stop at',
        'instructions' => 'Specify when station will stop broadcast.',
        'warning'      => 'When this stations will stop playing.',
    ],
    'status'           => [
        'name'   => 'Status',
        'option' => [
            'live'  => 'Online',
            'draft' => 'Draft',
            'dead'  => 'Offline',
            'wait'  => 'Wait',
        ],
    ],
];
