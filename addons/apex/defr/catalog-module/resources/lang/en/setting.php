<?php

return [
    'permalink_structure' => [
        'label'        => 'Permalink Structure',
        'instructions' => 'Customize the URL structure for your permalinks and archives. This can improve the aesthetics, usability, and forward-compatibility of your links.',
    ],
    'module_segment'      => [
        'label'        => 'Module Segment',
        'instructions' => 'Define a custom segment URI for the the Products module.',
    ],
    'category_segment'    => [
        'label'        => 'Category Segment',
        'instructions' => 'Define a custom category segment URI for the category URLs.',
    ],
    'tag_segment'         => [
        'label'        => 'Tag Segment',
        'instructions' => 'Define a custom tag segment for the tag URLs.',
    ],
    'products_per_page'   => [
        'label'        => 'Products Per Page',
        'instructions' => 'Define how many products to display per page on your website.',
    ],
    'ttl'                 => [
        'label'        => 'Time to live (TTL)',
        'instructions' => 'How long (in minutes) do you want to cache products before before serving fresh content?',
    ],
];
