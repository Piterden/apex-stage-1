<?php

return [
    'products'   => 'المنشورات',
    'fields'  => 'الحقول',
    'archive' => 'الأرشيف',
    'tagged'  => 'موسوم ":tag"',
];
