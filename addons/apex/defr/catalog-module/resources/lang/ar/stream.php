<?php

return [
    'products'      => [
        'name' => 'المنشورات',
    ],
    'product_types' => [
        'name' => 'أنواع المنشور',
    ],
    'categories' => [
        'name' => 'الأصناف',
    ],
];
