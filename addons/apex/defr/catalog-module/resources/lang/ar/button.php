<?php

return [
    'new_product'     => 'منشور جديد',
    'new_category' => 'صنف جديد',
    'new_type'     => 'نوع جديد',
    'new_field'    => 'حقل جديد',
    'add_field'    => 'اضافة حقل',
];
