<?php namespace Defr\CatalogModule\ProductUsage\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\View\ViewTemplate;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;

/**
 * Class AddProductUsageMetaTitle
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductUsageMetaTitle
{

    use DispatchesJobs;

    /**
     * The category instance.
     *
     * @var ProductUsageInterface
     */
    protected $category;

    /**
     * Create a new AddProductUsageMetaTitle instance.
     *
     * @param ProductUsageInterface $category
     */
    public function __construct(ProductUsageInterface $category)
    {
        $this->category = $category;
    }

    /**
     * Handle the command.
     *
     * @param ViewTemplate $template
     */
    public function handle(ViewTemplate $template)
    {
        $template->set('meta_title', $this->category->getName());
    }
}
