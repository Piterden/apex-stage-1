<?php namespace Defr\CatalogModule\ProductUsage\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\ProductUsage\Command\GetProductUsagePath;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddProductUsageBreadcrumb
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductUsageBreadcrumb
{

    use DispatchesJobs;

    /**
     * The category instance.
     *
     * @var ProductUsageInterface
     */
    protected $category;

    /**
     * Create a new AddProductUsageBreadcrumb instance.
     *
     * @param ProductUsageInterface $category
     */
    public function __construct(ProductUsageInterface $category)
    {
        $this->category = $category;
    }

    /**
     * Handle the command.
     *
     * @param BreadcrumbCollection $breadcrumbs
     */
    public function handle(BreadcrumbCollection $breadcrumbs)
    {
        $breadcrumbs->add(
            $this->category->getName(),
            $this->dispatch(new GetProductUsagePath($this->category))
        );
    }
}
