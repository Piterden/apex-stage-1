<?php namespace Defr\CatalogModule\ProductUsage;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\Entry\EntryPresenter;
use Defr\CatalogModule\ProductUsage\Command\GetProductUsagePath;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;

/**
 * Class ProductUsagePresenter
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductUsagePresenter extends EntryPresenter
{

    use DispatchesJobs;

    /**
     * The presented object.
     * This is for IDE support.
     *
     * @var ProductUsageInterface
     */
    protected $object;

    /**
     * Return the category path.
     *
     * @return string
     */
    public function path()
    {
        return $this->dispatch(new GetProductUsagePath($this->object));
    }
}
