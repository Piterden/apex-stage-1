<?php namespace Defr\CatalogModule\ProductUsage\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Interface ProductUsageRepositoryInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ProductUsageRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find a category by it's slug.
     *
     * @param  $slug
     * @return null|ProductUsageInterface
     */
    public function findBySlug($slug);
}
