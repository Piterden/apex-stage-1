<?php namespace Defr\CatalogModule\ProductType\Form;

/**
 * Class ProductTypeFormSections
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTypeFormSections
{

    /**
     * Handle the section.
     *
     * @param ProductTypeFormBuilder $builder
     */
    public function handle(ProductTypeFormBuilder $builder)
    {
        $builder->setSections([
            'general' => [
                'fields' => [
                    'name',
                    'slug',
                    'description',
                ],
            ],
            'info'    => [
                'fields' => [
                    'content',
                    'video',
                    'image',
                    'preview',
                    'meta_title',
                    'meta_description',
                    'meta_keywords',
                ],
            ],
            'layout'  => [
                'fields' => [
                    'theme_layout',
                    'layout',
                ],
            ],
        ]);
    }
}
