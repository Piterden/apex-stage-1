<?php namespace Defr\CatalogModule\ProductType\Command;

use Anomaly\Streams\Platform\Support\Presenter;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;

/**
 * Class GetProductType
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetProductType
{

    /**
     * The type identifier.
     *
     * @var mixed
     */
    protected $identifier;

    /**
     * Create a new GetProductType instance.
     *
     * @param $identifier
     */
    public function __construct($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Handle the command.
     *
     * @param  ProductTypeRepositoryInterface $types
     * @return ProductTypeInterface|null
     */
    public function handle(ProductTypeRepositoryInterface $types)
    {
        if (is_numeric($this->identifier))
        {
            return $types->find($this->identifier);
        }

        if (is_string($this->identifier))
        {
            return $types->findBySlug($this->identifier);
        }

        if ($this->identifier instanceof Presenter)
        {
            return $this->identifier->getObject();
        }

        if ($this->identifier instanceof ProductTypeInterface)
        {
            return $this->identifier;
        }

        return null;
    }
}
