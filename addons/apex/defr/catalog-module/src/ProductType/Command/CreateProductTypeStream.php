<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Config\Repository;
use Defr\CatalogModule\ProductType\ProductTypeModel;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;

/**
 * Class CreateProductTypeStream
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class CreateProductTypeStream
{

    /**
     * The product type instance.
     *
     * @var ProductTypeModel
     */
    protected $type;

    /**
     * Create a new CreateProductTypeStream instance.
     *
     * @param ProductTypeModel $type
     */
    public function __construct(ProductTypeModel $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command
     *
     * @param StreamRepositoryInterface $streams
     * @param Repository                $config
     */
    public function handle(
        StreamRepositoryInterface $streams,
        Repository $config
    )
    {
        $locale = $config->get('app.fallback_locale');

        $streams->create(
            [
                $locale        => [
                    'name'        => $this->type->getName(),
                    'description' => $this->type->getDescription(),
                ],
                'slug'         => $this->type->getSlug().'_products',
                'namespace'    => 'catalog',
                'locked'       => false,
                'translatable' => true,
                'trashable'    => true,
                'hidden'       => true,
            ]
        );
    }
}
