<?php namespace Defr\CatalogModule\ProductType\Command;

use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;

/**
 * Class GetProductTypeStream
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 */
class GetProductTypeStream
{

    /**
     * The product type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new GetProductTypeStream instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param  StreamRepositoryInterface $streams
     * @return \Anomaly\Streams\Platform\Stream\Contract\StreamInterface|null
     */
    public function handle(StreamRepositoryInterface $streams)
    {
        return $streams->findBySlugAndNamespace(
            $this->type->getSlug() . '_products', 'catalog'
        );
    }
}
