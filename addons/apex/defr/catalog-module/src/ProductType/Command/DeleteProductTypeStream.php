<?php namespace Defr\CatalogModule\ProductType\Command;

use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;


/**
 * Class DeleteProductTypeStream
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 */
class DeleteProductTypeStream
{

    /**
     * The product type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new DeleteProductTypeStream instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param StreamRepositoryInterface $streams
     */
    public function handle(StreamRepositoryInterface $streams)
    {
        if (!$this->type->isForceDeleting()) {
            return;
        }

        $streams->delete($streams->findBySlugAndNamespace(
            $this->type->getSlug() . '_products', 'catalog'
        ));
    }
}
