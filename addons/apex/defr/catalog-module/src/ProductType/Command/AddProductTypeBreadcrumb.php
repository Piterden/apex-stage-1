<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\ProductType\Command\GetProductTypePath;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddProductTypeBreadcrumb
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductTypeBreadcrumb
{

    use DispatchesJobs;

    /**
     * The type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new AddProductTypeBreadcrumb instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param BreadcrumbCollection $breadcrumbs
     */
    public function handle(BreadcrumbCollection $breadcrumbs)
    {
        $breadcrumbs->add(
            $this->type->getName(),
            $this->dispatch(new GetProductTypePath($this->type))
        );
    }
}
