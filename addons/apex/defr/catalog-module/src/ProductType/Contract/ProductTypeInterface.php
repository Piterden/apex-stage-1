<?php namespace Defr\CatalogModule\ProductType\Contract;

use Defr\CatalogModule\Product\ProductCollection;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;

/**
 * Interface ProductTypeInterface
 *
 * @package       Defr\CatalogModule\ProductType\Contract
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ProductTypeInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return integer The identifier.
     */
    public function getId();

    /**
     * Get the name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Get the description.
     *
     * @return string
     */
    public function getDescription();

    /**
     * Get the related stream.
     *
     * @return StreamInterface
     */
    public function getEntryStream();

    /**
     * Get the related stream's entry model.
     *
     * @return string
     */
    public function getEntryModel();

    /**
     * Get the CSS path.
     *
     * @return string
     */
    public function getCssPath();

    /**
     * Get the JS path.
     *
     * @return string
     */
    public function getJsPath();

    /**
     * Get related products.
     *
     * @return ProductCollection
     */
    public function getProducts();

    /**
     * Return the products relationship.
     *
     * @return HasMany
     */
    public function products();
}
