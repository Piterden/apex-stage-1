<?php namespace Defr\CatalogModule\ProductType;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Defr\CatalogModule\ProductType\Command\CleanUpRepeaters;
use Defr\CatalogModule\ProductType\Command\CreateRepeaterForType;
// use Defr\CatalogModule\ProductType\Command\CreateRepeatersForTypes;
use Defr\CatalogModule\ProductType\Command\GetTypeProperties;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class ProductTypeSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class ProductTypeSeeder extends Seeder
{

    use DispatchesJobs;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * The type repository.
     *
     * @var ProductTypeRepositoryInterface
     */
    protected $types;

    /**
     * The field repository.
     *
     * @var FieldRepositoryInterface
     */
    protected $fields;

    /**
     * The streams repository.
     *
     * @var StreamRepositoryInterface
     */
    protected $streams;

    /**
     * The assignment repository.
     *
     * @var AssignmentRepositoryInterface
     */
    protected $assignments;

    /**
     * Create a new ProductTypeSeeder instance.
     *
     * @param Repository                     $config
     * @param ProductTypeRepositoryInterface $types
     * @param FieldRepositoryInterface       $fields
     * @param StreamRepositoryInterface      $streams
     * @param AssignmentRepositoryInterface  $assignments
     */
    public function __construct(
        Repository $config,
        ProductTypeRepositoryInterface $types,
        FieldRepositoryInterface $fields,
        StreamRepositoryInterface $streams,
        AssignmentRepositoryInterface $assignments
    )
    {
        parent::__construct();

        $this->types       = $types;
        $this->config      = $config;
        $this->fields      = $fields;
        $this->streams     = $streams;
        $this->assignments = $assignments;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {

        echo "\n\n\033[34;5;228mStarting types seeder!\n";

        $types = $this->types->getLive()->mapWithKeys(
            function ($type)
            {
                $repository = new EntryRepository();
                $repository->setModel($type->getEntryModel());
                $repository->truncate();

                return [$type->getId() => $type];
            }
        );

        $this->types->truncate();
        // $repeaterTypes = [];

        echo "\033[35;5;228mProductTypes cleared!\n";

        // $this->dispatch(new CleanUpRepeaters());

        if ($data = file_get_contents(__DIR__.'/../../resources/seeders/product_types.json'))
        {
            $trans = json_decode(file_get_contents(__DIR__.'/../../resources/seeders/product_types_trans.json'), true);

            foreach (json_decode($data, true) as $type)
            {
                // $typeProperties  = [];
                // $typeName        = array_get($type, 'name');
                $typeSlug = array_get($type, 'slug');
                // $typeDescription = array_get($type, 'description');

                foreach (array_filter($trans, function ($transEntry) use ($type)
                {
                    return array_get($transEntry, 'entry_id') === array_get($type, 'id');
                }) as $translation)
                {
                    array_set($type, array_get($translation, 'locale'), $translation);
                }

                /* @var ProductTypeInterface $type */
                $type = $this->types->create($type);

                /* @var ProductTypeInterface $type */
                // $type = $this->types->create([
                //     'en'           => [
                //         'name'        => $typeName,
                //         'description' => $typeDescription,
                //     ],
                //     'ru'           => [
                //         'name'        => $typeName,
                //         'description' => $typeDescription,
                //     ],
                //     'slug'         => $typeSlug,
                //     'theme_layout' => 'theme::layouts/'.$typeSlug.'.twig',
                //     'layout'       => '{{ product.content.render|raw }}',
                //     'enabled'      => true,
                // ]);

                /* @var StreamInterface $stream */
                $stream = $type->getEntryStream();
                // $properties = $this->dispatch(new GetTypeProperties($type));

                // foreach ($properties as $key => $value)
                // {
                //     if (is_string($key))
                //     {
                //         array_set($typeProperties, $key, $value);
                //     }
                // }

                $this->assignments->create([
                    'stream' => $stream,
                    'field'  => $this->fields->findBySlugAndNamespace('content', 'catalog'),
                ]);

                // $this->dispatch(new CreateRepeaterForType($typeProperties, $type));
                // array_set($repeaterTypes, $typeName, $typeProperties);

                echo "\033[36;5;228mCreated type \033[31;5;228m".$typeSlug."\n";
            }

            echo "\033[32;5;228mProductTypes seeded successfully!\n";

            // $this->dispatch(new CreateRepeatersForTypes($repeaterTypes));
        }

        // if ($startProductTypes = $this->config->get('defr.module.catalog::seeder.types'))
        // {
        //     foreach ($startProductTypes as $slug => $name)
        //     {
        //         if ($type = $this->types->findBySlug($slug.'_products'))
        //         {
        //             $this->types->delete($type);
        //         }
        //     }

        //     $this->types->truncate();

        //     foreach ($startProductTypes as $slug => $name)
        //     {
        //         /* @var ProductTypeInterface $type */
        //         $type = $this->types->create([
        //             'en'           => [
        //                 'name'        => $name,
        //                 'description' => "A {$name} product type.",
        //             ],
        //             'slug'         => $slug,
        //             'theme_layout' => "theme::layouts/{$slug}.twig",
        //             'layout'       => '{{ product.content.render|raw }}',
        //         ]);

        //         $stream = $type->getEntryStream();

        //         $this->assignments->create([
        //             'stream' => $stream,
        //             'field'  => $this->fields->findBySlugAndNamespace('content', 'catalog'),
        //         ]);
        //     }
        // }
    }
}
