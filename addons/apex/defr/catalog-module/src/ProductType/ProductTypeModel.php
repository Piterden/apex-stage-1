<?php namespace Defr\CatalogModule\ProductType;

use Anomaly\Streams\Platform\Model\Catalog\CatalogProductTypesEntryModel;
use Anomaly\Streams\Platform\Stream\Contract\StreamInterface;
use Defr\CatalogModule\ProductType\Command\GetProductTypePath;
use Defr\CatalogModule\ProductType\Command\GetProductTypeStream;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\Product\ProductModel;

/**
 * Class ProductTypeModel
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTypeModel extends CatalogProductTypesEntryModel implements ProductTypeInterface
{

    /**
     * The cache time.
     *
     * @var int
     */
    protected $cacheMinutes = 99999;

    /**
     * Gets the identifier.
     *
     * @return integer The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the title.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->desciption;
    }

    /**
     * Gets the video.
     *
     * @return string The video.
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Get the related entry stream.
     *
     * @return StreamInterface
     */
    public function getEntryStream()
    {
        return $this->dispatch(new GetProductTypeStream($this));
    }

    /**
     * Get the related entry model name.
     *
     * @return EntryInretface
     */
    public function getEntryModel()
    {
        return $this->getEntryStream()->getEntryModel();
    }

    /**
     * Get the related entry model name.
     *
     * @return string
     */
    public function getEntryModelName()
    {
        $stream = $this->getEntryStream();

        return $stream->getEntryModelName();
    }

    /**
     * Get the CSS path.
     *
     * @return string
     */
    public function getCssPath()
    {
        /* @var EditorFieldProductType $css */
        $css = $this->getFieldType('css');

        $css->setEntry($this);

        return $css->getAssetPath();
    }

    /**
     * Get the JS path.
     *
     * @return string
     */
    public function getJsPath()
    {
        /* @var EditorFieldProductType $js */
        $js = $this->getFieldType('js');

        $js->setEntry($this);

        return $js->getAssetPath();
    }

    /**
     * Return the category's path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->dispatch(new GetProductTypePath($this));
    }

    /**
     * Get related products.
     *
     * @return ProductCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Return the products relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(ProductModel::class, 'type_id');
    }
}
