<?php namespace Defr\CatalogModule\ProductType;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\ProductType\ProductTypeCollection;
use Defr\CatalogModule\ProductType\ProductTypeModel;

/**
 * Class ProductTypeRepository
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTypeRepository extends EntryRepository implements ProductTypeRepositoryInterface
{

    /**
     * The type model.
     *
     * @var ProductTypeModel
     */
    protected $model;

    /**
     * Create a new ProductTypeRepository instance.
     *
     * @param ProductTypeModel $model
     */
    public function __construct(ProductTypeModel $model)
    {
        $this->model = $model;
    }

    /**
     * Gets all live types.
     *
     * @return ProductTypeCollection
     */
    public function getLive()
    {
        return $this->model
        ->where('deleted_at', null)
        ->where('enabled', true)
        ->get();
    }

    /**
     * Find a category by it's slug.
     *
     * @param  string                      $slug
     * @return null|ProductTypeInterface
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * Makes an array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection()
    {
        return $this->all()->map(
            /* @var ProductTypeInterface $type */
            function (ProductTypeInterface $type)
            {
                return array_merge($type->toArray(), [
                    'id'          => $type->getId(),
                    'name'        => $type->getName(),
                    'slug'        => $type->getSlug(),
                    'video'       => $type->getVideo(),
                    'description' => $type->getDescription(),
                ]);
            }
        );
    }
}
