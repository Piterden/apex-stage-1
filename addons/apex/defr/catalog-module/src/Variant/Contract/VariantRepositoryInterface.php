<?php namespace Defr\CatalogModule\Variant\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface VariantRepositoryInterface extends EntryRepositoryInterface
{

}
