<?php namespace Defr\CatalogModule\Variant\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class VariantFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'sku'      => [
            'unique'   => true,
            'required' => true,
        ],
        'color',
        'capacity' => [
            'options' => [
                'separator' => ',',
                'point'     => '.',
                'decimals'  => 2,
                'max'       => 10000,
            ],
        ],
        'value',
    ];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        'price',
    ];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'save',
    ];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

}
