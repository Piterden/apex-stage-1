<?php namespace Defr\CatalogModule;

use Anomaly\FilesFieldType\Table\FileTableBuilder;
use Anomaly\FilesFieldType\Table\ValueTableBuilder;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Catalog\CatalogBrandsEntryModel;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductsEntryModel;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductTypesEntryModel;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductUsagesEntryModel;
use Anomaly\Streams\Platform\Model\Catalog\CatalogPropertiesEntryModel;
use Anomaly\Streams\Platform\Model\Catalog\CatalogVariantsEntryModel;
use Defr\CatalogModule\Brand\BrandModel;
use Defr\CatalogModule\Brand\BrandRepository;
use Defr\CatalogModule\Brand\Contract\BrandRepositoryInterface;
use Defr\CatalogModule\File\Support\MultipleFieldType\LookupTableBuilder;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\ProductType\ProductTypeModel;
use Defr\CatalogModule\ProductType\ProductTypeRepository;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageRepositoryInterface;
use Defr\CatalogModule\ProductUsage\ProductUsageModel;
use Defr\CatalogModule\ProductUsage\ProductUsageRepository;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;
use Defr\CatalogModule\Product\ProductModel;
use Defr\CatalogModule\Product\ProductRepository;
use Defr\CatalogModule\Property\Contract\PropertyRepositoryInterface;
use Defr\CatalogModule\Property\PropertyModel;
use Defr\CatalogModule\Property\PropertyRepository;
use Defr\CatalogModule\Variant\Contract\VariantRepositoryInterface;
use Defr\CatalogModule\Variant\VariantModel;
use Defr\CatalogModule\Variant\VariantRepository;

class CatalogModuleServiceProvider extends AddonServiceProvider
{

    protected $bindings = [
        CatalogBrandsEntryModel::class        => BrandModel::class,
        CatalogProductsEntryModel::class      => ProductModel::class,
        CatalogProductTypesEntryModel::class  => ProductTypeModel::class,
        CatalogProductUsagesEntryModel::class => ProductUsageModel::class,
        CatalogVariantsEntryModel::class      => VariantModel::class,
        CatalogPropertiesEntryModel::class    => PropertyModel::class,
        FileTableBuilder::class               => LookupTableBuilder::class,
        ValueTableBuilder::class              => \Defr\CatalogModule\File\Support\MultipleFieldType\ValueTableBuilder::class,
    ];

    protected $singletons = [
        BrandRepositoryInterface::class        => BrandRepository::class,
        ProductRepositoryInterface::class      => ProductRepository::class,
        ProductTypeRepositoryInterface::class  => ProductTypeRepository::class,
        ProductUsageRepositoryInterface::class => ProductUsageRepository::class,
        VariantRepositoryInterface::class      => VariantRepository::class,
        PropertyRepositoryInterface::class     => PropertyRepository::class,
    ];

    protected $overrides = [
        'streams::partials/constants' => 'defr.module.catalog::admin/partials/constants',
        'streams::form/form'          => 'defr.module.catalog::admin/form',
    ];

    protected $routes = [
        'admin/catalog'                                                => 'Defr\CatalogModule\Http\Controller\Admin\ProductsController@index',
        'admin/catalog/ajax/choose_type'                               => 'Defr\CatalogModule\Http\Controller\Admin\ProductsController@chooseProductType',
        'admin/catalog/create'                                         => 'Defr\CatalogModule\Http\Controller\Admin\ProductsController@create',
        'admin/catalog/edit/{id}'                                      => 'Defr\CatalogModule\Http\Controller\Admin\ProductsController@edit',
        'admin/catalog/view/{id}'                                      => 'Defr\CatalogModule\Http\Controller\Admin\ProductsController@view',
        'admin/catalog/product_usages'                                 => 'Defr\CatalogModule\Http\Controller\Admin\ProductUsagesController@index',
        'admin/catalog/product_usages/create'                          => 'Defr\CatalogModule\Http\Controller\Admin\ProductUsagesController@create',
        'admin/catalog/product_usages/edit/{id}'                       => 'Defr\CatalogModule\Http\Controller\Admin\ProductUsagesController@edit',
        'admin/catalog/product_usages/view/{id}'                       => 'Defr\CatalogModule\Http\Controller\Admin\ProductUsagesController@view',
        'admin/catalog/brands'                                         => 'Defr\CatalogModule\Http\Controller\Admin\BrandsController@index',
        'admin/catalog/brands/create'                                  => 'Defr\CatalogModule\Http\Controller\Admin\BrandsController@create',
        'admin/catalog/brands/edit/{id}'                               => 'Defr\CatalogModule\Http\Controller\Admin\BrandsController@edit',
        'admin/catalog/brands/view/{id}'                               => 'Defr\CatalogModule\Http\Controller\Admin\BrandsController@view',
        'admin/catalog/types'                                          => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@index',
        'admin/catalog/types/create'                                   => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@create',
        'admin/catalog/types/edit/{id}'                                => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@edit',
        'admin/catalog/types/choose/{id}'                              => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@choose',
        'admin/catalog/types/assignments/{id}'                         => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@properties',
        'admin/catalog/types/assignments/{id}/assign/{property}'       => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@assign',
        'admin/catalog/types/assignments/{id}/assignment/{assignment}' => 'Defr\CatalogModule\Http\Controller\Admin\ProductTypesController@assignment',
        'admin/catalog/properties'                                     => 'Defr\CatalogModule\Http\Controller\Admin\FieldsController@index',
        'admin/catalog/properties/choose'                              => 'Defr\CatalogModule\Http\Controller\Admin\FieldsController@choose',
        'admin/catalog/properties/create'                              => 'Defr\CatalogModule\Http\Controller\Admin\FieldsController@create',
        'admin/catalog/properties/edit/{id}'                           => 'Defr\CatalogModule\Http\Controller\Admin\FieldsController@edit',
        'admin/catalog/variants'                                       => 'Defr\CatalogModule\Http\Controller\Admin\VariantsController@index',
        'admin/catalog/variants/create'                                => 'Defr\CatalogModule\Http\Controller\Admin\VariantsController@create',
        'admin/catalog/variants/edit/{id}'                             => 'Defr\CatalogModule\Http\Controller\Admin\VariantsController@edit',
    ];
}
