<?php namespace Defr\CatalogModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class CatalogModule extends Module
{

    /**
     * The module's icon.
     *
     * @var string
     */
    protected $icon = 'database';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'products'       => [
            'buttons' => [
                'new_product' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'href'        => 'admin/catalog/ajax/choose_type',
                ],
            ],
        ],
        'types'          => [
            'buttons'  => [
                'new_type',
                'assign_fields' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'enabled'     => 'admin/catalog/types/assignments/*',
                    'href'        => 'admin/catalog/types/choose/{request.route.parameters.id}',
                ],
            ],
            'sections' => [
                'assignments' => [
                    'hidden'  => true,
                    'href'    => 'admin/catalog/types/assignments/{request.route.parameters.type}',
                    'buttons' => [
                        'assign_properties' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#modal',
                            'href'        => 'admin/catalog/types/assignments/{request.route.parameters.type}/choose',
                        ],
                    ],
                ],
            ],
        ],
        'product_usages' => [
            'buttons' => [
                'new_category',
            ],
        ],
        'brands'         => [
            'buttons' => [
                'new_brand',
            ],
        ],
        'properties'  => [
            'buttons' => [
                'new_property' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#modal',
                    'href'        => 'admin/catalog/properties/choose',
                ],
            ],
        ],
    ];
}
