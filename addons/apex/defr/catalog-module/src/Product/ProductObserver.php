<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

class ProductObserver extends EntryObserver
{

    /**
     * Fired just before saving the entry.
     *
     * @param EntryInterface|ProductInterface $entry
     */
    public function creating(EntryInterface $entry)
    {
        if (!$entry->getStrId())
        {
            $entry->setAttribute('str_id', str_random());
        }

        parent::creating($entry);
    }
}
