<?php namespace Defr\CatalogModule\Product\Table\Filter;

use Illuminate\Database\Eloquent\Builder;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Contract\FilterInterface;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Query\GenericFilterQuery;

/**
 * Class SkuFilterQuery
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SkuFilterQuery extends GenericFilterQuery
{

    /**
     * Handle the filter query.
     *
     * @param Builder         $query
     * @param FilterInterface $filter
     */
    public function handle(Builder $query, FilterInterface $filter)
    {
        $query
        ->from('catalog_products_variants')
        ->leftJoin(
            'catalog_products',
            'catalog_products_variants.entry_id',
            '=',
            'catalog_products.id'
        )
        ->leftJoin(
            'catalog_variants',
            'catalog_variants.id',
            '=',
            'catalog_products_variants.related_id'
        )
        ->where('sku', 'like', '%'.$filter->getValue().'%');

        // dd($query->toSql());
    }
}
