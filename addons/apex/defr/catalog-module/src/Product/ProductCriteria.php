<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Entry\EntryCriteria;
use Defr\CatalogModule\ProductType\Command\GetProductType;

/**
 * Class ProductCriteria
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductCriteria extends EntryCriteria
{

    /**
     * Return only live.
     *
     * @return $this
     */
    public function live()
    {
        $this->query->where('enabled', true);
        $this->query->where('publish_at', '<=', date('Y-m-d H:i:s'));

        return $this;
    }

    /**
     * Return chronologically.
     *
     * @return $this
     */
    public function recent()
    {
        $this->live();

        $this->query->orderBy('publish_at', 'DESC');

        return $this;
    }

    /**
     * Return only featured.
     *
     * @return $this
     */
    public function featured()
    {
        $this->recent();

        $this->query->where('featured', true);

        return $this;
    }

    /**
     * Add the type constraint.
     *
     * @param  $identifier
     * @return $this
     */
    public function type($identifier)
    {
        /* @var ProductTypeInterface $type */
        $type = $this->dispatch(new GetProductType($identifier));

        $stream = $type->getEntryStream();
        $table  = $stream->getEntryTableName();

        $this->query
        ->select('catalog_products.*')
        ->where('type_id', $type->getId())
        ->join($table.' AS entry', 'entry.id', '=', 'products_products.entry_id');

        return $this;
    }
}
