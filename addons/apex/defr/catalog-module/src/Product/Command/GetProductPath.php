<?php namespace Defr\CatalogModule\Product\Command;

use Anomaly\Streams\Platform\Support\Value;
use Illuminate\Contracts\Config\Repository;
use Anomaly\Streams\Platform\Support\Resolver;
use Anomaly\Streams\Platform\Support\Evaluator;
use Defr\CatalogModule\Product\Contract\ProductInterface;

/**
 * Class GetProductPath
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetProductPath
{

    /**
     * The product instance.
     *
     * @var ProductInterface
     */
    protected $product;

    /**
     * Return the path for a product.
     *
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Handle the command.
     *
     * @param  Repository $config
     * @param  Resolver   $resolver
     * @param  Evaluator  $evaluator
     * @param  Value      $value
     * @return string
     */
    public function handle(
        Repository $config,
        Resolver $resolver,
        Evaluator $evaluator,
        Value $value
    )
    {
        $base = '/'.$config->get('defr.module.catalog::paths.module');

        if (!$this->product->isLive())
        {
            return $base.'/preview/'.$this->product->getStrId();
        }

        return $base.'/'.$value->make(
            $evaluator->evaluate(
                $resolver->resolve(
                    $config->get('defr.module.catalog::paths.permalink'),
                    ['product' => $this->product]
                ),
                ['product' => $this->product]
            ),
            $this->product
        );
    }
}
