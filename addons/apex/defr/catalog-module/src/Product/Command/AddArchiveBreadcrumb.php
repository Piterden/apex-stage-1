<?php namespace Defr\CatalogModule\Product\Command;

use Illuminate\Http\Request;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddArchiveBreadcrumb
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddArchiveBreadcrumb
{

    /**
     * Handle the command.
     *
     * @param Request              $request
     * @param BreadcrumbCollection $breadcrumbs
     */
    public function handle(Request $request, BreadcrumbCollection $breadcrumbs)
    {
        $breadcrumbs->add(
            trans('module::breadcrumb.archive'),
            $request->fullUrl()
        );
    }
}
