<?php namespace Defr\CatalogModule\Product\Contract;

use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\Brand\Contract\BrandInterface;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;
use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ProductRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find a product by it's slug.
     *
     * @param  $product
     * @return null|ProductInterface
     */
    public function findBySlug($slug);

    /**
     * Find a product by it's string ID.
     *
     * @param  $id
     * @return null|ProductInterface
     */
    public function findByStrId($id);

    /**
     * Find many products by tag.
     *
     * @param  $tag
     * @param  null             $limit
     * @return ProductCollection
     */
    public function findManyByTag($tag, $limit = null);

    /**
     * Find many products by category.
     *
     * @param  ProductUsageInterface $category
     * @param  null              $limit
     * @return ProductCollection
     */
    public function findManyByProductUsage(ProductUsageInterface $category, $limit = null);

    /**
     * Find many products by brand.
     *
     * @param  BrandInterface    $brand
     * @param  null              $limit
     * @return EntryCollection
     */
    public function findManyByBrand(BrandInterface $brand, $limit = null);

    /**
     * Find many products by type.
     *
     * @param  ProductTypeInterface    $type
     * @param  null             $limit
     * @return ProductCollection
     */
    public function findManyByProductType(ProductTypeInterface $type, $limit = null);

    /**
     * Find many products by date.
     *
     * @param  $year
     * @param  $month
     * @param  null             $limit
     * @return ProductCollection
     */
    public function findManyByDate($year, $month, $limit = null);

    /**
     * Get recent products.
     *
     * @param  null             $limit
     * @return ProductCollection
     */
    public function getRecent($limit = null);

    /**
     * Get featured products.
     *
     * @param  null             $limit
     * @return ProductCollection
     */
    public function getFeatured($limit = null);

    /**
     * Get live products.
     *
     * @return ProductCollection
     */
    public function getLive();
}
