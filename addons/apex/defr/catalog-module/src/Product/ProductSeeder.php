<?php namespace Defr\CatalogModule\Product;

use Illuminate\Config\Repository;
use Anomaly\FilesModule\File\FileUploader;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\CatalogModule\Brand\Contract\BrandRepositoryInterface;
use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageRepositoryInterface;

/**
 * Class ProductSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class ProductSeeder extends Seeder
{

    use DispatchesJobs;

    /**
     * The config repository
     *
     * @var Repository
     */
    protected $config;

    /**
     * Uploader
     *
     * @var FileUploader
     */
    protected $uploader;

    /**
     * The brands repository.
     *
     * @var BrandRepositoryInterface
     */
    protected $brands;

    /**
     * The folders repository.
     *
     * @var FolderRepositoryInterface
     */
    protected $folders;

    /**
     * The products repository.
     *
     * @var ProductRepositoryInterface
     */
    protected $products;

    /**
     * The types repository.
     *
     * @var ProductTypeRepositoryInterface
     */
    protected $types;

    /**
     * The usages repository.
     *
     * @var ProductUsageRepositoryInterface
     */
    protected $usages;

    /**
     * Create a new ProductSeeder instance.
     *
     * @param Repository                      $config
     * @param ProductTypeRepositoryInterface  $types
     * @param BrandRepositoryInterface        $brands
     * @param ProductRepositoryInterface      $products
     * @param ProductUsageRepositoryInterface $usages
     */
    public function __construct(
        Repository $config,
        FileUploader $uploader,
        BrandRepositoryInterface $brands,
        FolderRepositoryInterface $folders,
        StreamRepositoryInterface $streams,
        ProductRepositoryInterface $products,
        ProductTypeRepositoryInterface $types,
        ProductUsageRepositoryInterface $usages
    )
    {
        parent::__construct();

        $this->config   = $config;
        $this->uploader = $uploader;
        $this->brands   = $brands;
        $this->folders  = $folders;
        $this->streams  = $streams;
        $this->products = $products;
        $this->types    = $types;
        $this->usages   = $usages;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        // $imagesPath   = __DIR__.'/../../resources/seeders/images/';
        // $imagesJson   = __DIR__.'/../../resources/seeders/images.json';
        $productsJson      = __DIR__.'/../../resources/seeders/products.json';
        $productsTransJson = __DIR__.'/../../resources/seeders/products_trans.json';

        echo "\n\033[37;5;228mStarting products seeder!\n";

        $this->products->truncate();

        echo "\033[35;5;228mProducts cleared!\n";

        if ($data = file_get_contents($productsJson))
        {
            // if (!$images = file_get_contents($imagesJson))
            // {
            //     echo "\033[35;5;228mImages is empty!\n";

            //     return;
            // }

            // $images = collect(json_decode($images, true));
            // $brand  = $this->brands->findBySlug('body');
            // $folder = $this->folders->find(1);

            $trans = json_decode(file_get_contents($productsTransJson), true);

            foreach (json_decode($data, true) as $key => $product)
            {
                // $commandResponse = $this->dispatch(
                //     new GetPropertiesFromHtmlTable(array_get($product, 'content'))
                // );

                // $html = array_get($commandResponse, 'full');
                // $type = $this->types->find(array_get($product, 'type_id'));

                // $entry = $type->getEntryModel()->create([
                //     'content' => str_replace($html, '', array_get($product, 'content')),
                //     // 'content' => str_replace($html, '', array_get($product, 'content')),
                // ]);
                $productTitle = array_get($product, 'slug');

                foreach (array_filter($trans, function ($transEntry) use ($product)
                {
                    return array_get($transEntry, 'entry_id') === array_get($product, 'id');
                }) as $translation)
                {
                    array_set($product, array_get($translation, 'locale'), $translation);
                }

                $this->products->create(array_except($product, ['sku', 'capacity']));

                // if ($properties = array_get($commandResponse, 'props'))
                // {
                //     $properties = collect($properties)->mapWithKeys(
                //         function ($prop, $key)
                //         {
                //             return [str_slug($key) => $prop];
                //         }
                //     )->toArray();

                //     $repeater = $this->streams->findBySlugAndNamespace(
                //         $type->getSlug(), 'repeater'
                //     );
                //     $repeaterEntry = $repeater->getEntryModel();

                //     $repeater->setConfigAttribute([
                //         // 'related' => 'Anomaly\Streams\Platform\Model\Repeater\Repeater'.ucfirst($type->getSlug()).'EntryModel',
                //         'related' => class_basename($repeaterEntry),
                //     ]);

                //     foreach ($properties as $key => $value)
                //     {
                //         $repeaterEntry->setFieldValue($key, $value);
                //     }
                // }

                // if ($image = $images->where('image_id', $product['image_id'])->first())
                // {
                //     $error = trans('anomaly.module.files::error.generic');

                //     $file = $imagesPath.$image['image_path'];

                //     $uploaded = new UploadedFile(
                //         $file,
                //         array_get($image, 'image_name'),
                //         array_get($image, 'mime_type'),
                //         filesize($file),
                //         null,
                //         true
                //     );

                //     if ($preview = $images->where(
                //         'image_id',
                //         array_get($product, 'tn_image_id')
                //     )->first())
                //     {
                //         $previewFile = $imagesPath.array_get($preview, 'image_path');

                //         $previewUploaded = new UploadedFile(
                //             $previewFile,
                //             array_get($preview, 'image_name'),
                //             array_get($preview, 'mime_type'),
                //             filesize($previewFile),
                //             null,
                //             true
                //         );
                //     }

                //     if ($file = $this->uploader->upload($uploaded, $folder))
                //     {
                // $productTitle     = array_get($productTrans, 'title');
                // $productSummary   = array_get($productTrans, 'summary');
                // // $productSku       = array_get($product, 'sku');
                // $productEnabled   = array_get($product, 'enabled');
                // $productFeatured  = array_get($product, 'featured');
                // $productSortOrder = array_get($product, 'sort_order');

                // $productProps = [
                //     'en'         => [
                //         'title'   => $productTitle,
                //         'summary' => $productSummary,
                //     ],
                //     'ru'         => [
                //         'title'   => $productTitle,
                //         'summary' => $productSummary,
                //     ],
                //     'slug'       => str_slug($productTitle).'-'.$key,
                //     'type'       => $type,
                //     'brand'      => $brand,
                //     'entry'      => $entry,
                //     'image'      => $file,
                //     'preview'    => $preview,
                //     'sku'        => $productSku,
                //     'enabled'    => $productEnabled,
                //     'featured'   => $productFeatured,
                //     'sort_order' => $productSortOrder,
                // ];

                // if ($properties)
                // {
                //     array_set($productProps, 'properties', $repeater);
                // }

                // if ($sku = array_get($product, 'sku', false))
                // {
                //     array_set($productProps, 'sku', $sku);
                // }

                // if ($tags = array_get($product, 'tags', false))
                // {
                //     array_set($productProps, 'tags', $tags);
                // }

                // if ($advantages = array_get($product, 'advantages', false))
                // {
                //     array_set($productProps, 'advantages', $advantages);
                // }

                // if ($for_whom = array_get($product, 'for_whom', false))
                // {
                //     array_set($productProps, 'for_whom', $for_whom);
                // }

                // if ($advices = array_get($product, 'advices', false))
                // {
                //     array_set($productProps, 'advices', $advices);
                // }

                // if ($attentions = array_get($product, 'attentions', false))
                // {
                //     array_set($productProps, 'attentions', $attentions);
                // }

                // if (isset($previewUploaded))
                // {
                //     array_set(
                //         $productProps,
                //         'preview',
                //         $this->uploader->upload($previewUploaded, $folder)
                //     );
                // }

                /* @var ProductInterface $product */
                // $product = $this->products->create($productProps);

                echo "\033[36;5;228mCreated product \033[31;5;228m".$productTitle."\n";
                //     }
                // }

            }

            echo "\033[32;5;228mProducts seeded successfully!\n";
        }

        // if ($products = $this->config->get('defr.module.catalog::seeder.products'))
        // {
        //     $repository = new EntryRepository();
        //     $repository->setModel(new CatalogShpaklevkiProductsEntryModel());
        //     $repository->truncate();

        //     $type     = $this->types->findBySlug('shpaklevki');
        //     $brand    = $this->brands->findBySlug('body');
        //     $category = $this->usages->findBySlug('garazh');

        //     foreach ($products as $product)
        //     {
        //         $welcome = (new CatalogShpaklevkiProductsEntryModel())->create([
        //             'content' => "<p>{$product['ru']['title']} - содержимое.</p>",
        //         ]);

        // $this->products->create(array_merge($product, [
        //     'type'     => $type,
        //     'brand'    => $brand,
        //     'entry'    => $welcome,
        //     //'category' => $category,
        // ]));
        //     }

        //     $this->command->getOutput()->writeln("<info>Products seeding done!</info>");
        // }
    }
}
// $type     = $this->types->findBySlug('default');
// $category = $this->usages->findBySlug('garazh');

// $welcome = (new CatalogDefaultProductsEntryModel())->create(
//     [
//         'content' => '<p>Welcome to PyroCMS!</p>',
//     ]
// );

// $this->products->create(
//     [
//         'en'         => [
//             'title'   => 'Welcome to PyroCMS!',
//             'summary' => 'This is an example product to demonstrate the products module.',
//         ],
//         'slug'       => 'welcome-to-pyrocms',
//         'publish_at' => time(),
//         'enabled'    => true,
//         'type_id'       => $type->id,
//         'entry_id'      => $welcome->id,
//         'category_id'   => $category->id,
//         'author_id'     => 1,
//     ]
// );
