<?php namespace Defr\CatalogModule\Product\Form;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class ProductFormFields
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductFormFields
{

    /**
     * Handle the form fields.
     *
     * @param Guard              $auth
     * @param ProductFormBuilder $builder
     */
    public function handle(Guard $auth, ProductFormBuilder $builder)
    {

        $builder->setFields(
            [
                '*',
                'author'     => [
                    'config' => [
                        'default_value' => $auth->id(),
                    ],
                ],
                'publish_at' => [
                    'config' => [
                        'default_value' => new Carbon('now'),
                    ],
                ],
            ]
        );
    }
}
