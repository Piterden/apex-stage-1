<?php namespace Defr\CatalogModule\Product\Form;

use Carbon\Carbon;
use Defr\CatalogModule\Product\ProductModel;
use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class ProductEntryFormSections
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductEntryFormSections
{

    /**
     * Handle the form sections.
     *
     * @param ProductEntryFormBuilder $builder
     */
    public function handle(ProductEntryFormBuilder $builder)
    {
        $builder->setSections([
            'general' => [
                'tabs' => [
                    'main'     => [
                        'title'  => 'defr.module.catalog::tab.main',
                        'fields' => [
                            'product_title',
                            'product_slug',
                            'product_summary',
                            'product_brand',
                            'product_tags',
                            'product_product_usages',
                        ],
                    ],
                    'media'    => [
                        'title'  => 'defr.module.catalog::tab.media',
                        'fields' => [
                            'product_image',
                            'product_preview',
                            'product_video',
                            'product_meta_title',
                            'product_meta_keywords',
                            'product_meta_description',
                        ],
                    ],
                    'fields'   => [
                        'title'  => 'defr.module.catalog::tab.fields',
                        'fields' => function (ProductEntryFormBuilder $builder)
                        {
                            return array_map(
                                function (FieldType $field)
                                {
                                    return 'entry_'.$field->getField();
                                },
                                array_filter(
                                    $builder->getFormFields()->base()->all(),
                                    function (FieldType $field)
                                    {
                                        return (!$field->getEntry() instanceof ProductModel);
                                    }
                                )
                            );
                        },
                    ],
                    'variants' => [
                        'title'  => 'defr.module.catalog::tab.variants',
                        'fields' => [
                            'product_variants',
                        ],
                    ],
                    'properties' => [
                        'title'  => 'defr.module.catalog::tab.properties',
                        'fields' => [
                            'product_properties',
                        ],
                    ],
                    'options'  => [
                        'title'  => 'defr.module.catalog::tab.options',
                        'fields' => [
                            'product_enabled',
                            'product_featured',
                            'product_new',
                            'product_advantages',
                            'product_for_whom',
                            'product_advices',
                            'product_attentions',
                            'product_author',
                            'product_publish_at',
                        ],
                    ],
                ],
            ],
        ]);
    }
}
