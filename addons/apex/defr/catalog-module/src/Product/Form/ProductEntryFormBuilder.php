<?php namespace Defr\CatalogModule\Product\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Defr\CatalogModule\Entry\Form\EntryFormBuilder;
use Anomaly\Streams\Platform\Ui\Form\Multiple\MultipleFormBuilder;

/**
 * Class ProductEntryFormBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductEntryFormBuilder extends MultipleFormBuilder
{

    protected $assets = [
        'theme.css' => [
            'defr.module.catalog::sass/module.scss',
        ],
    ];

    /**
     * Fired after the entry form is saved.
     *
     * After the entry form is saved take the
     * entry and use it to populate the product
     * before it saves directly after.
     *
     * @param EntryFormBuilder $builder
     */
    public function onSavedEntry(EntryFormBuilder $builder)
    {
        /* @var FormBuilder $form */
        $form = $this->forms->get('product');

        $product = $form->getFormEntry();

        $entry = $builder->getFormEntry();

        $product->entry_id   = $entry->getId();
        $product->entry_type = get_class($entry);
    }

    /**
     * Get the contextual entry ID.
     *
     * @return int|mixed|null
     */
    public function getContextualId()
    {
        /* @var FormBuilder $form */
        $form = $this->forms->get('product');

        return $form->getContextualId();
    }
}
