<?php namespace Defr\CatalogModule\Product\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;

/**
 * Class ProductFormBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductFormBuilder extends FormBuilder
{

    protected $stacked = true;

    /**
     * The product type.
     *
     * @var null|ProductTypeInterface
     */
    protected $type = null;

    /**
     * The skipped fields.
     *
     * @var array
     */
    protected $skips = [
        'type',
        'entry',
        'str_id',
    ];

    /**
     * Fired when the builder is ready to build.
     *
     * @throws \Exception
     */
    public function onReady()
    {
        if (!$this->getType() && !$this->getEntry())
        {
            throw new \Exception('The $type parameter is required when creating a product.');
        }
    }

    /**
     * Fired just before saving the form.
     */
    public function onSaving()
    {
        $entry = $this->getFormEntry();
        $type  = $this->getType();

        // $this->dispatch(new AddProductToRepeaters($this));

        if (!$entry->type_id)
        {
            $entry->type_id = $type->getId();
        }
    }

    /**
     * Get the type.
     *
     * @return ProductTypeInterface|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the type.
     *
     * @param  ProductTypeInterface $type
     * @return $this
     */
    public function setProductType(ProductTypeInterface $type)
    {
        $this->type = $type;

        return $this;
    }
}
