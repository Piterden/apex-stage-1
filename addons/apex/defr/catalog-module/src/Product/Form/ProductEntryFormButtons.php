<?php namespace Defr\CatalogModule\Product\Form;

use Carbon\Carbon;
use Defr\CatalogModule\Product\ProductModel;
use Anomaly\Streams\Platform\Addon\FieldType\FieldType;

/**
 * Class ProductEntryFormButtons
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductEntryFormButtons
{

    /**
     * Handle the form buttons.
     *
     * @param ProductEntryFormBuilder $builder
     */
    public function handle(ProductEntryFormBuilder $builder)
    {
        $model = $builder->getChildFormEntry('product');

        $builder->setButtons([
            'view' => [
                'href'       => 'admin/catalog/view/'.$model->getId(),
                'attributes' => [
                    'target' => '_blank',
                ],
            ],
        ]);
    }
}
