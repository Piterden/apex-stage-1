<?php namespace Defr\CatalogModule\Product;

use Defr\CatalogModule\Product\Contract\ProductInterface;
use Anomaly\Streams\Platform\Support\Authorizer;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class ProductAuthorizer
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 */
class ProductAuthorizer
{

    /**
     * The authorization utility.
     *
     * @var Guard
     */
    protected $guard;

    /**
     * The authorizer utility.
     *
     * @var Authorizer
     */
    protected $authorizer;

    /**
     * Create a new ProductAuthorizer instance.
     *
     * @param Guard      $guard
     * @param Authorizer $authorizer
     */
    public function __construct(Guard $guard, Authorizer $authorizer)
    {
        $this->guard      = $guard;
        $this->authorizer = $authorizer;
    }

    /**
     * Authorize the product.
     *
     * @param ProductInterface $product
     */
    public function authorize(ProductInterface $product)
    {
        if (!$product->isEnabled() &&
            !$this->authorizer->authorize('defr.module.catalog::view_drafts')) {
            abort(404);
        }
    }
}
