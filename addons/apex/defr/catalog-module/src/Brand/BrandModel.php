<?php namespace Defr\CatalogModule\Brand;

use Anomaly\Streams\Platform\Model\Catalog\CatalogBrandsEntryModel;
use Defr\CatalogModule\Brand\Command\GetBrandPath;
use Defr\CatalogModule\Brand\Contract\BrandInterface;

class BrandModel extends CatalogBrandsEntryModel implements BrandInterface
{

    /**
     * Get the title.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Return the category's path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->dispatch(new GetBrandPath($this));
    }

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl()
    {
        if ($this->image)
        {
            $filename   = $this->image->name;
            $foldername = strtolower($this->image->folder->slug);
            $diskname   = strtolower($this->image->disk->name);

            return url("app/apex/files-module/{$diskname}/{$foldername}/{$filename}");
        }

        return '';
    }

    /**
     * Gets the button text.
     *
     * @return string The button text.
     */
    public function getButtonText()
    {
        return $this->button_text;
    }

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription()
    {
        return $this->description;
    }
}
