<?php namespace Defr\CatalogModule\Brand\Command;

use Illuminate\Contracts\Config\Repository;
use Defr\CatalogModule\Brand\Contract\BrandInterface;

/**
 * Class GetBrandPath
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetBrandPath
{

    /**
     * The brand instance.
     *
     * @var BrandInterface
     */
    protected $brand;

    /**
     * Create a new GetBrandPath instance.
     *
     * @param BrandInterface $brand
     */
    public function __construct(BrandInterface $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Handle the command.
     *
     * @param  Repository $config
     * @return string
     */
    public function handle(Repository $config)
    {
        $module = $config->get('defr.module.catalog::paths.module');
        $brand  = $config->get('defr.module.catalog::paths.brand');
        $slug   = $this->brand->getSlug();

        return "/{$module}/{$brand}/{$slug}";
    }
}
