<?php namespace Defr\CatalogModule\Brand\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface BrandInterface extends EntryInterface
{

    /**
     * Get the name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl();

    /**
     * Gets the button text.
     *
     * @return string The button text.
     */
    public function getButtonText();

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription();
}
