<?php namespace Defr\CatalogModule\Brand\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface BrandRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find a brand by it's slug.
     *
     * @param  $slug
     * @return null|BrandInterface
     */
    public function findBySlug($slug);

    /**
     * Makes array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection();
}
