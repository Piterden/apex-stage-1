<?php namespace Defr\CatalogModule\Brand;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class BrandPresenter extends EntryPresenter
{

    /**
     * Get thumb of image
     *
     * @return string
     */
    public function imageLabel()
    {
        return
        '<div>
            <img height="50px" src="'.$this->object->getImageUrl().'" />
        </div>';
    }
}
