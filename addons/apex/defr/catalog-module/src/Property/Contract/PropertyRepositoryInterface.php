<?php namespace Defr\CatalogModule\Property\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface PropertyRepositoryInterface extends EntryRepositoryInterface
{

}
