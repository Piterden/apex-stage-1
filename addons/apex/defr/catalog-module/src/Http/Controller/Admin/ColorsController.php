<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Defr\CatalogModule\Color\Form\ColorFormBuilder;
use Defr\CatalogModule\Color\Table\ColorTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ColorsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ColorTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ColorTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ColorFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ColorFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ColorFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ColorFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
