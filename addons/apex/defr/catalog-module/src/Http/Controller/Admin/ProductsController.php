<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Illuminate\Routing\Redirector;
use Anomaly\Streams\Platform\Support\Authorizer;
use Defr\CatalogModule\Product\Command\GetProductPath;
use Defr\CatalogModule\Product\Table\ProductTableBuilder;
use Defr\CatalogModule\Product\Form\ProductEntryFormBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;
use Defr\CatalogModule\Product\Form\Command\AddEntryFormFromProduct;
use Defr\CatalogModule\Product\Form\Command\AddEntryFormFromRequest;
use Defr\CatalogModule\Product\Form\Command\AddProductFormFromProduct;
use Defr\CatalogModule\Product\Form\Command\AddProductFormFromRequest;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;

class ProductsController extends AdminController
{

    /**
     * Return a table of existing products.
     *
     * @param  ProductTableBuilder         $table
     * @return \Illuminate\Http\Response
     */
    public function index(ProductTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Return the form for creating a new product.
     *
     * @param  ProductEntryFormBuilder                      $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ProductEntryFormBuilder $form)
    {
        $this->dispatch(new AddEntryFormFromRequest($form));
        $this->dispatch(new AddProductFormFromRequest($form));

        return $form->render();
    }

    /**
     * Return the form for editing an existing product.
     *
     * @param  ProductRepositoryInterface                   $products
     * @param  ProductEntryFormBuilder                      $form
     * @param  $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ProductRepositoryInterface $products, ProductEntryFormBuilder $form, $id)
    {
        $product = $products->find($id);

        $this->dispatch(new AddEntryFormFromProduct($form, $product));
        $this->dispatch(new AddProductFormFromProduct($form, $product));

        return $form->render($id);
    }

    /**
     * Redirect to a product's URL.
     *
     * @param  ProductRepositoryInterface          $products
     * @param  Redirector                          $redirect
     * @param  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function view(ProductRepositoryInterface $products, Redirector $redirect, $id)
    {
        /* @var ProductInterface $product */
        $product = $products->find($id);

        if (!$product->isLive())
        {
            return $redirect->to($this->dispatch(new GetProductPath($product)));
        }

        return $redirect->to($product->path());
    }

    /**
     * Delete a product and go back.
     *
     * @param  ProductRepositoryInterface          $products
     * @param  Authorizer                          $authorizer
     * @param  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(ProductRepositoryInterface $products, Authorizer $authorizer, $id)
    {
        $authorizer->authorize('defr.module.catalog::products.delete');

        $products->delete($products->find($id));

        return redirect()->back();
    }

    /**
     * Return the modal for choosing a product type.
     *
     * @param  ProductTypeRepositoryInterface $types
     * @return \Illuminate\View\View
     */
    public function chooseProductType(ProductTypeRepositoryInterface $types)
    {
        return view('defr.module.catalog::admin/ajax/choose_type', ['types' => $types->all()]);
    }
}
