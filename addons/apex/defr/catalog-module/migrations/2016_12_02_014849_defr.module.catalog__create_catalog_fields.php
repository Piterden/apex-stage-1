<?php

use Anomaly\UsersModule\User\UserModel;
use Defr\CatalogModule\Brand\BrandModel;
use Anomaly\SelectFieldType\Handler\Layouts;
use Defr\CatalogModule\Product\ProductModel;
use Defr\CatalogModule\Variant\VariantModel;
use Defr\CatalogModule\Property\PropertyModel;
use Defr\CatalogModule\ProductType\ProductTypeModel;
use Defr\CatalogModule\ProductUsage\ProductUsageModel;
use Anomaly\Streams\Platform\Database\Migration\Migration;

// use Anomaly\Streams\Platform\Repeater\RepeaterDefaultEntryModel;

class DefrModuleCatalogCreateCatalogFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'str_id'           => 'anomaly.field_type.text',
        'name'             => 'anomaly.field_type.text',
        'title'            => 'anomaly.field_type.text',
        'button_text'      => 'anomaly.field_type.text',
        'slug'             => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'title',
                'type'    => '-',
            ],
        ],
        'content'          => [
            'type'   => 'anomaly.field_type.wysiwyg',
            'locked' => 0, // Used with seeded pages.
        ],
        'summary'          => 'anomaly.field_type.textarea',
        'description'      => 'anomaly.field_type.textarea',
        'preview'          => 'anomaly.field_type.file',
        'image'            => 'anomaly.field_type.file',
        'brand'            => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'mode'    => 'lookup',
                'related' => BrandModel::class,
            ],
        ],
        'type'             => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => ProductTypeModel::class,
            ],
        ],
        'product_usage'    => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'mode'    => 'lookup',
                'related' => ProductUsageModel::class,
            ],
        ],
        'product_usages'   => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'mode'    => 'lookup',
                'related' => ProductUsageModel::class,
            ],
        ],
        'author'           => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'mode'          => 'lookup',
                'related'       => UserModel::class,
                'default_value' => 1,
            ],
        ],
        'variants'         => [
            'type'   => 'anomaly.field_type.repeater',
            'config' => [
                'manage'  => false,
                'add_row' => 'defr.module.catalog::button.add_variant',
                'related' => VariantModel::class,
            ],
        ],
        'properties'       => [
            'type'   => 'anomaly.field_type.repeater',
            'config' => [
                'manage'  => false,
                'add_row' => 'defr.module.catalog::button.add_property',
                'related' => PropertyModel::class,
            ],
        ],
        'product'          => [
            'type'   => 'anomaly.field_type.multiple',
            'config' => [
                'mode'    => 'lookup',
                'related' => ProductModel::class,
            ],
        ],
        'value'            => 'anomaly.field_type.text',
        'sku'              => 'anomaly.field_type.text',
        'price'            => 'anomaly.field_type.decimal',
        'theme_layout'     => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'handler' => Layouts::class,
            ],
        ],
        'video'            => 'anomaly.field_type.video',
        'new'              => 'anomaly.field_type.boolean',
        'show'             => 'anomaly.field_type.boolean',
        'publish_at'       => 'anomaly.field_type.datetime',
        'layout'           => [
            'type'   => 'anomaly.field_type.editor',
            'config' => [
                'default_value' => '{{ post.content|raw }}',
                'mode'          => 'twig',
            ],
        ],
        'enabled'          => [
            'type'   => 'anomaly.field_type.boolean',
            'config' => [
                'default_value' => false,
            ],
        ],
        'featured'         => [
            'type'   => 'anomaly.field_type.boolean',
            'config' => [
                'default_value' => false,
            ],
        ],
        'featured_at'      => 'anomaly.field_type.datetime',
        'color'            => [
            'type'   => 'anomaly.field_type.colorpicker',
            'config' => [
                'default_value' => '#fff',
                'format'        => 'hex',
            ],
        ],
        'capacity'         => [
            'type'   => 'anomaly.field_type.decimal',
            'config' => [
                'separator' => ' ',
                'point'     => '.',
                'decimals'  => 3,
                'step'      => 0.1,
            ],
        ],
        'tags'             => 'anomaly.field_type.tags',
        'advantages'       => 'anomaly.field_type.tags',
        'for_whom'         => 'anomaly.field_type.tags',
        'advices'          => 'anomaly.field_type.tags',
        'attentions'       => 'anomaly.field_type.tags',
        'meta_title'       => 'anomaly.field_type.text',
        'meta_description' => 'anomaly.field_type.textarea',
        'meta_keywords'    => 'anomaly.field_type.tags',
        'entry'            => 'anomaly.field_type.polymorphic',
        'ttl'              => [
            'type'   => 'anomaly.field_type.integer',
            'config' => [
                'min'  => 0,
                'step' => 1,
                'page' => 5,
            ],
        ],
    ];

}
