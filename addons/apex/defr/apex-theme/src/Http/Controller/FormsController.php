<?php namespace Defr\ApexTheme\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Defr\ApexModule\Question\Contract\QuestionRepositoryInterface;
use Defr\ApexModule\Recall\Contract\RecallRepositoryInterface;
use Defr\ApexModule\Subscription\Contract\SubscriptionRepositoryInterface;
use Illuminate\Support\Facades\Validator;

/**
 * FormsController class
 */
class FormsController extends PublicController
{

    /**
     * Recall request handle
     *
     * @param  RecallRepositoryInterface $recalls The recalls
     * @return Response
     */
    public function recall(RecallRepositoryInterface $recalls)
    {
        if (!$this->request->ajax())
        {
            return redirect('/');
        }

        $validator = Validator::make($this->request->all(), [
            'name'  => 'required',
            'phone' => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        if ($recall = $recalls->create($this->request->only(['name', 'phone'])))
        {
            return $this->goodResponse($recall);
        }

        return $this->badResponse('Can\'t create');
    }

    /**
     * Newsletter subscribe
     *
     * @param  SubscriptionRepositoryInterface $subscriptions The subscriptions
     * @return Response
     */
    public function subscribe(SubscriptionRepositoryInterface $subscriptions)
    {
        if (!$this->request->ajax())
        {
            return redirect('/');
        }

        $validator = Validator::make($this->request->all(), [
            'email' => 'required|email|unique:apex_subscriptions',
        ]);

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        if ($sub = $subscriptions->create($this->request->only(['email'])))
        {
            return $this->goodResponse($sub);
        }

        return $this->badResponse('Can\'t create');
    }

    /**
     * Newsletter unsubscribe
     *
     * @param  SubscriptionRepositoryInterface $subscriptions The subscriptions
     * @return Response
     */
    public function unsubscribe(SubscriptionRepositoryInterface $subscriptions)
    {
        if (!$this->request->ajax())
        {
            return redirect('/');
        }

        $validator = Validator::make($this->request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        if ($sub = $subscriptions->findByEmail($this->request->get('email')))
        {
            if ($subscriptions->delete($sub))
            {
                return $this->goodResponse($sub);
            }

            return $this->badResponse('Can\'t delete');
        }

        return $this->badResponse('Can\'t find subscription');
    }

    /**
     * Question from user
     *
     * @param  QuestionRepositoryInterface $questions The questions
     * @return Response
     */
    public function question(QuestionRepositoryInterface $questions)
    {
        if (!$this->request->ajax())
        {
            return redirect('/');
        }

        $validator = Validator::make($this->request->all(), [
            'email'   => 'required|email',
            'name'    => 'required',
            'city'    => 'required',
            'phone'   => 'required',
            'message' => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        if ($question = $questions->create($this->request->only(
            [
                'email',
                'name',
                'city',
                'phone',
                'message',
            ]
        )))
        {
            return $this->goodResponse($question);
        }

        return $this->badResponse();
    }

    /**
     * Returns error response
     *
     * @param  string     $error The error
     * @return Response
     */
    private function badResponse($error = '')
    {
        return response()->json([
            'success' => false,
            'error'   => $error,
            'request' => $this->request->all(),
        ], 400);
    }

    /**
     * Returns error response
     *
     * @param  array      $entry The entry
     * @return Response
     */
    private function goodResponse($entry = [])
    {
        return response()->json([
            'success' => true,
            'data'    => $entry,
            'request' => $this->request->all(),
        ], 200);
    }
}
