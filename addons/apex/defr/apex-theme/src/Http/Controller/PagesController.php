<?php namespace Defr\ApexTheme\Http\Controller;

use Anomaly\FilesModule\Folder\Contract\FolderRepositoryInterface;
use Anomaly\NavigationModule\Menu\Contract\MenuRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Defr\ApexModule\Banner\Contract\BannerRepositoryInterface;
use Defr\ApexModule\Review\Contract\ReviewRepositoryInterface;
use Defr\CatalogModule\Brand\Contract\BrandRepositoryInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageRepositoryInterface;
use Illuminate\Contracts\Config\Repository;
use Websemantics\CalendarModule\Calendar\Event\Contract\EventRepositoryInterface;

/**
 * Class PagesController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class PagesController extends PublicController
{
    /**
     * Main vue-app view
     *
     * @param  ProductUsageRepositoryInterface $categories
     * @param  ProductTypeRepositoryInterface  $types
     * @param  BannerRepositoryInterface       $banners
     * @param  ReviewRepositoryInterface       $reviews
     * @param  FolderRepositoryInterface       $folders
     * @param  BrandRepositoryInterface        $brands
     * @param  ColorRepositoryInterface        $colors
     * @param  EventRepositoryInterface        $events
     * @param  MenuRepositoryInterface         $menus
     * @param  Repository                      $config
     * @return Response
     */
    public function view(
        ProductUsageRepositoryInterface $categories,
        ProductTypeRepositoryInterface $types,
        BannerRepositoryInterface $banners,
        ReviewRepositoryInterface $reviews,
        FolderRepositoryInterface $folders,
        BrandRepositoryInterface $brands,
        EventRepositoryInterface $events,
        MenuRepositoryInterface $menus,
        Repository $config
    )
    {
        $view = view('theme::layouts.default', [
            'messages'   => [
                'error'   => $this->messages->get('error'),
                'success' => $this->messages->get('success'),
                'info'    => $this->messages->get('info'),
                'warning' => $this->messages->get('warning'),
            ],
            'menus'      => $menus->all()->map(
                function ($menu)
                {
                    $out = $menu->toArray();

                    $out['links'] = $menu->getLinks();

                    return $out;
                }
            ),
            'categories' => $categories->toVueCollection(),
            'banners'    => $banners->toVueCollection(),
            'reviews'    => $reviews->toVueCollection(),
            'brands'     => $brands->toVueCollection(),
            'events'     => $events->all(),
            'config'     => $config->get('app'),
            'types'      => $types->toVueCollection(),
        ]);

        $this->messages->flush();

        return response($view, 200);
    }
}
