<?php namespace Defr\ApexTheme\Http\Controller;

use Illuminate\Routing\Redirector;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;

/**
 * Class PostsController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 *
 * @link          http://pyrocms.com/
 */
class PostsController extends AdminController
{

    /**
     * Redirect to a post's URL.
     *
     * @param  PostRepositoryInterface             $posts
     * @param  Redirector                          $redirect
     * @param  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function view(PostRepositoryInterface $posts, Redirector $redirect, $id)
    {
        /* @var PostInterface $post */
        $post = $posts->find($id);

        if (!$post->isLive())
        {
            return $redirect->to('/')->with(['preview' => ['post' => $post]]);
        }

        return $redirect->to('blog/post/'.$post->getSlug());
    }
}
