<?php namespace Defr\ApexTheme\Http\Controller\Api;

use Anomaly\FilesModule\File\Contract\FileRepositoryInterface;
use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;

// use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;

class PostsApiController extends RestController
{

    /**
     * Files repository
     *
     * @var FileRepositoryInterface
     */
    public $files;

    /**
     * Create PostsApiController instance
     *
     * @param PostRepositoryInterface $repository
     * @param FileRepositoryInterface $files
     */
    public function __construct(
        PostRepositoryInterface $repository,
        FileRepositoryInterface $files
    )
    {
        $this->repository = $repository;
        $this->files      = $files;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* @var  PostCollection  $post */
        $collection = $this->repository->getRecent()->map(
            /* @var  PostModel  $post */
            function ($post)
            {
                $entry = $post->getEntry();

                if ($imageId = $entry->image_id)
                {
                    $image = $this->files->find($imageId);

                    $post->image_url = $image->url();
                }

                $post->content = $post->content();

                return $post;
            }
        );

        return $this->makeResponse($collection);
    }
}
