<?php namespace Defr\ApexTheme\Http\Controller\Api;

use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

class ProductsApiController extends RestController
{

    protected $tags = [
        'tags',
        'advantages',
        'for_whom',
        'advices',
        'attentions',
    ];

    protected $repeaters = [
        'properties',
        'variants',
    ];

    protected $relations = [
        'entry',
        'brand',
        'type',
    ];

    protected $files = [
        'image',
        'preview',
    ];

    protected $entryFiles = [
        'tds',
        'msds',
    ];

    protected $entryObjects = [
        'poverhnost',
    ];

    protected $except = [
        'str_id',
        'entry_type',
    ];

    /**
     * Create an instance of ProductsApiController
     *
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* @var  ProductCollection  $product */
        $collection = $this->repository->getLive()->map(
            /* @var  ProductModel  $product */
            function ($product)
            {
                $out = $product->toArray();

                array_set($out, 'content', $product->getContent());
                array_set($out, 'icons', $product->getIcons());

                foreach ($this->files as $field)
                {
                    $method = 'get'.ucfirst(camel_case($field)).'Url';

                    array_set($out, $field.'_url', $product->$method());
                }

                foreach ($this->relations as $field)
                {
                    $method = 'get'.ucfirst(camel_case($field));

                    array_set($out, $field, $product->$method());
                }

                foreach ($this->tags as $field)
                {
                    $method = 'get'.ucfirst(camel_case($field));

                    array_set($out, $field, $product->$method());
                }

                foreach ($this->repeaters as $field)
                {
                    $method = 'get'.ucfirst(camel_case($field));

                    array_set($out, $field.'_array', $product->$method());
                }

                foreach ($this->entryFiles as $field)
                {
                    $method = 'get'.ucfirst(camel_case($field)).'Url';

                    array_set($out['entry'], $field.'_url', $product->$method());
                }

                foreach ($this->entryObjects as $field)
                {
                    array_set($out, $field, $product->getEntry()->$field);
                }

                foreach ($this->except as $field)
                {
                    unset($out[$field]);
                }

                return $out;
            }
        );

        return $this->makeResponse($collection);
    }
}
