<?php namespace Defr\ApexTheme\Http\Controller\Api;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class RestController extends PublicController
{
    /**
     * Response status code
     *
     * @var integer
     */
    protected $code = 200;

    /**
     * Entry Repository
     *
     * @var EntryRepositoryInterface
     */
    protected $repository;

    /**
     * Create an instance of RestController
     *
     * @param EntryRepositoryInterface $repository [description]
     */
    public function __construct(EntryRepositoryInterface $repository)
    {
        parent::__construct();

        $this->repository = $repository;

        if (!$this->request->ajax())
        {
            return redirect('/');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!$collection = $this->repository->all())
        {
            $this->setCode(400);

            return $this->makeResponse();
        }

        return $this->makeResponse($collection);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->setCode(405);

        return $this->makeResponse();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->setCode(405);

        return $this->makeResponse();
    }

    /**
     * Display the specified resource.
     *
     * @param  int                         $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        if (!$entry = $this->repository->findBySlug($slug))
        {
            $this->setCode(400);

            return $this->makeResponse();
        }

        return $this->makeResponse($entry);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->setCode(405);
        return $this->makeResponse();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->setCode(405);
        return $this->makeResponse();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                         $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->setCode(405);
        return $this->makeResponse();
    }

    /**
     * Get response status code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set response status code
     *
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Construct JSON response
     *
     * @param  Collection $collection
     * @return Response
     */
    public function makeResponse($collection)
    {
        if (!isset($collection) || !$collection)
        {
            $collection = collect([]);
        }

        return response()->json($collection, $this->getCode());
    }

    /**
     * Get model allowed roles
     *
     * @param  EntryModel $model
     * @return array      of integers
     */
    public function getAllowedRoles($model)
    {
        return DB::table($model->getTableName().'_allowed_roles')
        ->where('entry_id', $model->id)
        ->get()
        ->pluck('related_id')
        ->toArray();
    }

    /**
     * Get current user
     *
     * @return UserModel|null
     */
    public function getUser()
    {
        return Auth::user();
    }

    /**
     * Exclude not allowed by ACL resources
     *
     * @param  EntryCollection   $entries
     * @return EntryCollection
     */
    public function filterNotAllowedEntries($entries)
    {
        /* @var EntryInterface */
        return $entries->filter(function ($entry)
        {
            $guestGroupId  = 3;
            $allowed_roles = $this->getAllowedRoles($entry);

            if (!count($allowed_roles) || in_array($guestGroupId, $allowed_roles))
            {
                return true;
            }

            /* @var UserInterface */
            if ($user = $this->getUser())
            {
                foreach ($user->getRoles()->pluck('id') as $roleId)
                {
                    if (in_array($roleId, $allowed_roles))
                    {
                        return true;
                    }
                }
            }
            return false;
        });
    }

}
