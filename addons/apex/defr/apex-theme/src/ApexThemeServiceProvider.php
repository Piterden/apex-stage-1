<?php namespace Defr\ApexTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Defr\ApexTheme\Http\Controller\Api\PostsApiController;
use Defr\ApexTheme\Http\Controller\Api\ProductsApiController;
use Defr\ApexTheme\Http\Controller\PagesController;
use Defr\ApexTheme\Http\Middleware\Cors;
use Illuminate\Routing\Router;

class ApexThemeServiceProvider extends AddonServiceProvider
{

    /**
     * App forms routes
     *
     * @var array
     */
    protected $routes = [
        'rest/recall'           => 'Defr\ApexTheme\Http\Controller\FormsController@recall',
        'rest/subscribe'        => 'Defr\ApexTheme\Http\Controller\FormsController@subscribe',
        'rest/unsubscribe'      => 'Defr\ApexTheme\Http\Controller\FormsController@unsubscribe',
        'rest/question'         => 'Defr\ApexTheme\Http\Controller\FormsController@question',
        'admin/posts/view/{id}' => 'Defr\ApexTheme\Http\Controller\PostsController@view',
    ];

    protected $routeMiddleware = [
        'cors' => Cors::class,
    ];

    /**
     * Map REST API Routes
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        $router->group([
            'prefix'     => 'rest',
            'middleware' => ['cors'],
        ], function ($router)
        {
            $router->resource('posts', PostsApiController::class);
            $router->resource('products', ProductsApiController::class);
        });
    }

    /**
     * Generate VueJS routes.
     *
     * @return array
     */
    public function getRoutes()
    {
        $routes = [
            'oauthcb' => [
                'as'   => 'OauthCb.vue',
                'uses' => PagesController::class.'@view',
                'verb' => 'any',
            ],
        ];

        foreach (glob($this->addon->getPath('resources/js/pages/*')) as $route)
        {
            $skip       = false;
            $middleware = [];
            $verb       = 'any';

            $filename = array_reverse(explode('/', $route))[0];

            if (ends_with($filename, 'Page.vue'))
            {
                $route = snake_case(str_replace_last('Page.vue', '', $filename));

                switch ($route)
                {
                    case 'index':
                        $route = '/';
                        break;

                    case 'about_apex':
                    case 'about_body':
                    case 'about_iwata':
                        $route = implode('/', explode('_', $route));
                        break;

                    case 'blog_tag':
                    case 'blog_post':
                    case 'blog_category':
                    case 'catalog_tag':
                    case 'catalog_type':
                    case 'catalog_usage':
                    case 'catalog_product':
                        $route = implode('/', explode('_', $route)).'/{slug}';
                        break;

                    case 'search':
                        $route .= '/{query}';
                        break;

                    case 'error404':
                        $skip = true;
                        break;
                }

                if (!$skip)
                {
                    $routes[$route] = [
                        'as'         => $filename,
                        'uses'       => PagesController::class.'@view',
                        'middleware' => $middleware,
                        'verb'       => $verb,
                    ];
                }
            }
        }

        return array_merge($this->routes, $routes);
    }
}
