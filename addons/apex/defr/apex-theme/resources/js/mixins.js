export const ListItemStyle = {

  methods: {
    /**
     * Make system url of image
     *
     * @param  String path
     * @return String
     */
    makeImageUrl (path) {
      return '/app/apex/files-module/local/' + path
    },

    /**
     * Make item style object
     *
     * @param  Object item
     * @return object
     */
    makeItemStyle (item) {
      const path = item && item.image_obj && item.image_obj.path
      return {
        backgroundImage: 'url(\'' + this.makeImageUrl(path) + '\')',
        backgroundSize: '100%',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center'
      }
    }
  }
}

export const MenuMixin = {

  props: {
    menu: Object
  },

  methods: {

    /**
     * Fixing URL in menu link for use in VueRouter
     *
     * @param  String url
     * @return String fixed url
     */
    fixUrl (url = '') {
      return url.replace(window.location.origin, '') || '/'
    },

    /**
     * Gets the menu.
     *
     * @param      {String}  slug    The slug
     * @return     {Object}  The menu.
     */
    getMenu (slug) {
      return this.$root.$children[0].menus.find(menu => {
        return menu.slug === slug
      })
    },

    /**
     * Gets the links.
     *
     * @param      {Object}  menu    The menu
     * @return     {Array}  The links.
     */
    getLinks (menu) {
      return menu && menu.links || []
    }
  },

  computed: {
    links () {
      return this.getLinks(this.menu)
    }
  }
}
