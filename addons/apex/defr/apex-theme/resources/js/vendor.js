/* eslint semi: 0 */
/* eslint-disable no-extend-native */
// Check for jQuery.
// TODO: Fix $(dom).velocity is not function bug
if (typeof (jQuery) === 'undefined') {
  if (typeof (require) === 'function') {
    window.jQuery = window.$ = require('jquery')
  } else {
    window.jQuery = $
  }
}

require('materialize-css/js/initial')
require('materialize-css/js/jquery.easing.1.3')
require('materialize-css/js/animation')
window.jQuery.velocity = window.Vel = require('materialize-css/js/velocity.min')
require('materialize-css/js/hammer.min')
require('materialize-css/js/jquery.hammer')
require('materialize-css/js/global')
require('materialize-css/js/toasts')
require('materialize-css/js/tabs')
require('materialize-css/js/transitions')

import 'materialize-css/dist/js/materialize.js'
// require('materialize-css/bin/materialize')

/**
 * Strip HTML-tags from string
 *
 * @return     {String}
 */
if (!String.prototype.stripTags) {
  Object.assign(String.prototype, {
    stripTags () {
      return this.replace(/<(((.*?\n+?.*?)+?|.*?)?)>/gm, '')
    }
  })
}
