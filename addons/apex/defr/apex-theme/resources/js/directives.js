import Vue from 'vue'

export const square = Vue.directive('square', {
  bind(el, binding) {
    window.addEventListener('resize', () => {
      el.style.height = Number(el.clientWidth * (binding.value || 1)) + 'px'
    })
  },
  inserted(el, binding) {
    el.style.height = Number(el.clientWidth * (binding.value || 1)) + 'px'
  },
  unbind(el, binding) {
    window.removeEventListener('resize', () => {
      el.style.height = Number(el.clientWidth * (binding.value || 1)) + 'px'
    })
  }
})
