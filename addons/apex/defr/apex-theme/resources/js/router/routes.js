import Vue from 'vue'

import BlogPage from '../pages/BlogPage.vue'
import IndexPage from '../pages/IndexPage.vue'
import LearnPage from '../pages/LearnPage.vue'
import SearchPage from '../pages/SearchPage.vue'
import CatalogPage from '../pages/CatalogPage.vue'
import BlogTypePage from '../pages/BlogTypePage.vue'
import BlogPostPage from '../pages/BlogPostPage.vue'
import PartnersPage from '../pages/PartnersPage.vue'
import AboutApexPage from '../pages/AboutApexPage.vue'
import AboutBodyPage from '../pages/AboutBodyPage.vue'
import SearchTagPage from '../pages/SearchTagPage.vue'
import AboutIwataPage from '../pages/AboutIwataPage.vue'
import AdMaterialsPage from '../pages/AdMaterialsPage.vue'
import CatalogTypePage from '../pages/CatalogTypePage.vue'
import BlogCategoryPage from '../pages/BlogCategoryPage.vue'
import CatalogUsagePage from '../pages/CatalogUsagePage.vue'
import CatalogProductPage from '../pages/CatalogProductPage.vue'
import CatalogCategoryPage from '../pages/CatalogCategoryPage.vue'

/**
 * Router config
 * @param  {String}   mode
 * @param  {String}   linkActiveClass
 * @param  {Array}    routes
 */
export default [{
  name: 'home.index',
  path: '/',
  meta: {
    name: 'Главная'
  },
  component: resolve => {
    return resolve(Vue.component('index-page', IndexPage))
  }
}, {
  name: 'learn.index',
  path: '/learn',
  meta: {
    name: 'Учебный центр'
  },
  component: resolve => {
    return resolve(Vue.component('learn-page', LearnPage))
  }
}, {
  name: 'about.apex',
  path: '/about/apex',
  meta: {
    name: 'О компании'
  },
  component: resolve => {
    return resolve(Vue.component('about-apex-page', AboutApexPage))
  }
}, {
  name: 'about.body',
  path: '/about/body',
  meta: {
    name: 'Body'
  },
  component: resolve => {
    return resolve(Vue.component('about-body-page', AboutBodyPage))
  }
}, {
  name: 'about.iwata',
  path: '/about/iwata',
  meta: {
    name: 'Iwata'
  },
  component: resolve => {
    return resolve(Vue.component('about-iwata-page', AboutIwataPage))
  }
}, {
  name: 'blog.index',
  path: '/blog',
  meta: {
    name: 'Новости'
  },
  component: resolve => {
    return resolve(Vue.component('blog-page', BlogPage))
  }
}, {
  name: 'blog.category',
  path: '/blog/category/:slug',
  meta: {
    name: 'Категория блога'
  },
  component: resolve => {
    return resolve(Vue.component('blog-category-page', BlogCategoryPage))
  }
}, {
  name: 'blog.type',
  path: '/blog/type/:slug',
  meta: {
    name: 'Тип статьи'
  },
  component: resolve => {
    return resolve(Vue.component('blog-type-page', BlogTypePage))
  }
}, {
  name: 'blog.post',
  path: '/blog/post/:slug',
  meta: {
    name: 'Статья {title}'
  },
  component: resolve => {
    return resolve(Vue.component('blog-post-page', BlogPostPage))
  }
}, {
  name: 'catalog.index',
  path: '/catalog',
  meta: {
    name: 'Каталог'
  },
  component: resolve => {
    return resolve(Vue.component('catalog-page', CatalogPage))
  }
}, {
  name: 'catalog.category',
  path: '/catalog/category/:slug',
  meta: {
    name: 'Категория каталога'
  },
  component: resolve => {
    return resolve(Vue.component('catalog-category-page', CatalogCategoryPage))
  }
}, {
  name: 'catalog.type',
  path: '/catalog/type/:slug',
  meta: {
    name: 'Тип товара'
  },
  component: resolve => {
    return resolve(Vue.component('catalog-type-page', CatalogTypePage))
  }
}, {
  name: 'catalog.usage',
  path: '/catalog/usage/:slug',
  meta: {
    name: 'Область применения'
  },
  component: resolve => {
    return resolve(Vue.component('catalog-usage-page', CatalogUsagePage))
  }
}, {
  name: 'catalog.product',
  path: '/catalog/product/:slug',
  meta: {
    name: 'Продукт {title}'
  },
  component: resolve => {
    return resolve(Vue.component('catalog-product-page', CatalogProductPage))
  }

}, {
  name: 'partners.index',
  path: '/partners',
  meta: {
    name: 'Партнерам'
  },
  component: resolve => {
    return resolve(Vue.component('partners-page', PartnersPage))
  }
}, {
  name: 'ad.materials',
  path: '/ads',
  meta: {
    name: 'Рекламные материалы'
  },
  component: resolve => {
    return resolve(Vue.component('ad-materials-page', AdMaterialsPage))
  }
}, {
  name: 'search.query',
  path: '/search/:query',
  meta: {
    name: 'Результаты поиска'
  },
  component: resolve => {
    return resolve(Vue.component('search-page', SearchPage))
  }
}, {
  name: 'search.tag',
  path: '/search/:tag',
  meta: {
    name: 'С тегом'
  },
  component: resolve => {
    return resolve(Vue.component('search-tag-page', SearchTagPage))
  }
}]

