require('./vendor')

import Vue from 'vue'

// Vue.config.silent = false
// Vue.config.devTools = true

// import store from './store'
import App from './App'
import VueMask from 'v-mask'
import router from './router'
import http from './plugins/http'
import eventbus from './plugins/eventbus'
import collection from './plugins/collection'

Vue.use(http)
Vue.use(VueMask)
Vue.use(eventbus)
Vue.use(collection)

new Vue({
  components: { App },
  // store, // injects the Store into all components
  router, // make Router work with the application
  el: '#App',
  data () {
    return {
      csrfToken: CSRF_TOKEN,
      phone: {
        simple: '8-800-555-07-76',
        coloured: '<span class=\"accent\">8-800</span>-555-07-76',
        link: 'tel:+78005550776',
        suffix: 'Бесплатно по России'
      },
      email: {
        address: 'mail@ap-ex.ru',
        link: 'mailto:mail@ap-ex.ru'
      },
      youtube: {
        userId: 'IERPTdT3Cyru7zvGqegt5g',
        channel: 'HBBODYRUS',
        channelId: 'UCIERPTdT3Cyru7zvGqegt5g',
        apiKey: 'AIzaSyBHowH_aFC9j5OpWtfrRSnRBEl1B5i55Fo'
      },
      fetching: false
    }
  },

  created () {
    this.bindEventListeners()
  },

  methods: {
    bindEventListeners () {
      let vm = this
      this.$bus.$on('main::setFetching', () => {
        vm.fetching = true
      })
      this.$bus.$on('main::endFetching', () => {
        vm.fetching = false
      })
    }
  }
})

