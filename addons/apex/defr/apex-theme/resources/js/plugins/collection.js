const Collection = (() => {
  function Collection () {
    let collection = Object.create(Array.prototype)

    collection = (Array.apply(collection, arguments) || collection)
    Collection.injectClassMethods(collection)

    return (collection)
  }

  Collection.injectClassMethods = collection => {
    let method

    for (method in Collection.prototype) {
      if (Collection.prototype.hasOwnProperty(method)) {
        collection[method] = Collection.prototype[method]
      }
    }

    return (collection)
  }

  Collection.fromArray = array => {
    let collection = Collection.apply(null, array)
    return (collection)
  }

  Collection.isArray = value => {
    let stringValue = Object.prototype.toString.call(value)
    return (stringValue.toLowerCase() === '[object array]')
  }

  Collection.prototype = {
    /**
     * Get featured elements of collection.
     *
     * @param   {Number}     count   The count
     * @return  {Collection}
     */
    featured (count) {
      return this.filter(item => {
        return item.featured
      }).sort((a, b) => {
        return a.sort_order > b.sort_order
      }).slice(0, count || 1)
    },

    /**
     * Get filtered collection
     *
     * @param   {Object}    filters  The filters
     * @return  {Collection}
     */
    filtered (filters = {}) {
      let out = [], name

      for (name in filters) {
        if (filters.hasOwnProperty(name)) {
          let value = filters[name]

          out.concat(this.filter(item => {
            if (Array.isArray(value)) {
              return value.includes(item.id)
            }
            if (typeof value === 'string') {
              return value === item[name]
            }
          }))
        }
      }

      return out.length && out || this
    },

  }

  return (Collection)
}).call({})

export default function install (Vue) {
  Object.defineProperties(Vue.prototype, {
    $collection: {
      get () {
        return (Collection)
      },
    }
  })
}
