export const MAIN_SET_TOKEN = 'MAIN_SET_TOKEN'
export const MAIN_SET_MESSAGE = 'MAIN_SET_MESSAGE'
export const MAIN_SET_FETCHING = 'MAIN_SET_FETCHING'

export const POST_LOAD = 'POST_LOAD'
export const POSTS_PULL = 'POSTS_PULL'
export const POSTS_PUSH = 'POSTS_PUSH'
export const POST_UNLOAD = 'POST_UNLOAD'
export const POST_UPDATE = 'POST_UPDATE'
export const POSTS_UPDATE = 'POSTS_UPDATE'

export const PRODUCT_LOAD = 'PRODUCT_LOAD'
export const PRODUCTS_PULL = 'PRODUCTS_PULL'
export const PRODUCTS_PUSH = 'PRODUCTS_PUSH'
export const PRODUCT_UNLOAD = 'PRODUCT_UNLOAD'
export const PRODUCT_UPDATE = 'PRODUCT_UPDATE'
export const PRODUCTS_UPDATE = 'PRODUCTS_UPDATE'

// export const USER_UPDATE = 'USER_UPDATE'
