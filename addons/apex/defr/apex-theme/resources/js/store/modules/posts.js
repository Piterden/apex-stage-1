import Vue from 'vue'
import * as TYPES from '../types'

const state = {
  isLoaded: false,
  posts: [],
  post: null
}

const mutations = {
  [TYPES.POST_LOAD](state, post) {
    Vue.set(state, 'post', post)
  },
  [TYPES.POST_UNLOAD](state) {
    Vue.set(state, 'post', null)
  },
  [TYPES.POST_UPDATE](state, post) {
    let idx = state.posts.findIndex(item => post.id === item.id)
    if (idx >= 0) {
      Vue.set(state.posts, idx, post)
      if (state.post && state.post.id === post.id) {
        Vue.set(state, 'post', post)
      }
    } else {
      Vue.set(state.posts, state.posts.length, post)
    }
  },
  [TYPES.POSTS_PUSH](state, post) {
    Vue.set(state.posts, state.posts.length, post)
  },
  [TYPES.POSTS_PULL](state, post) {
    state.posts.splice(state.posts.findIndex(item => post.id === item.id), 1)
    if (state.post && state.post.id === post.id) {
      Vue.set(state, 'post', null)
    }
  },
  [TYPES.POSTS_UPDATE](state, posts) {
    Vue.set(state, 'posts', posts)
    Vue.set(state, 'isLoaded', true)
  }
}

const actions = {
  navigateToPost ({ commit }, post) {
    commit(TYPES.POST_LOAD, post)
  },
  navigateToParent ({ commit }) {
    commit(TYPES.POST_UNLOAD)
  },
  fetch ({ commit }) {
    Vue.$http.get('posts')
      .then((response) => {
        commit(TYPES.POSTS_UPDATE, response.data)
      })
      .catch((reason) => {
        commit(TYPES.MAIN_SET_MESSAGE, reason)
      })
  }
  // POST_LOAD({ commit }, post) {
  //   commit(POST_LOAD, post)
  // },
  // POST_UNLOAD({ commit }) {
  //   commit(POST_UNLOAD)
  // },
  // POST_UPDATE({ commit }, post) {
  //   commit(POST_UPDATE, post)
  // },
  // POSTS_PUSH({ commit }, post) {
  //   commit(POSTS_PUSH, post)
  // },
  // POSTS_PULL({ commit }, post) {
  //   commit(POSTS_PULL, post)
  // },
  // POSTS_UPDATE({ commit }, posts) {
  //   commit(POSTS_UPDATE, posts)
  // }
}

export default {
  mutations,
  actions,
  state
}
