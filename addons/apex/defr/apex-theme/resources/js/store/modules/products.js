import Vue from 'vue'
import * as TYPES from '../types'

const state = {
  isLoaded: false,
  products: [],
  product: null
}

const mutations = {
  [TYPES.PRODUCT_LOAD](state, product) {
    Vue.set(state, 'product', product)
  },
  [TYPES.PRODUCT_UNLOAD](state) {
    Vue.set(state, 'product', null)
  },
  [TYPES.PRODUCT_UPDATE](state, product) {
    let idx = state.products.findIndex(item => product.id === item.id)
    if (idx >= 0) {
      Vue.set(state.products, idx, product)
      if (state.product && state.product.id === product.id) {
        Vue.set(state, 'product', product)
      }
    } else {
      Vue.set(state.products, state.products.length, product)
    }
  },
  [TYPES.PRODUCTS_PUSH](state, product) {
    Vue.set(state.products, state.products.length, product)
  },
  [TYPES.PRODUCTS_PULL](state, product) {
    state.products.splice(state.products.findIndex(item => product.id === item.id), 1)
    if (state.product && state.product.id === product.id) {
      Vue.set(state, 'product', null)
    }
  },
  [TYPES.PRODUCTS_UPDATE](state, products) {
    Vue.set(state, 'products', products)
    Vue.set(state, 'isLoaded', true)
  }
}

const actions = {
  navigateToProduct({ commit }, product) {
    commit(TYPES.PRODUCT_LOAD, product)
  },
  navigateToParent({ commit }) {
    commit(TYPES.PRODUCT_UNLOAD)
  },
  fetch ({ commit }) {
    Vue.$http.get('products')
      .then((response) => {
        commit(TYPES.PRODUCTS_UPDATE, response.data)
      })
      .catch((reason) => {
        commit(TYPES.MAIN_SET_MESSAGE, reason)
      })
  }
  // PRODUCT_UPDATE({ commit }, product) {
  //   commit(TYPES.PRODUCT_UPDATE, product)
  // },
  // PRODUCTS_PUSH({ commit }, product) {
  //   commit(TYPES.PRODUCTS_PUSH, product)
  // },
  // PRODUCTS_PULL({ commit }, product) {
  //   commit(TYPES.PRODUCTS_PULL, product)
  // },
  // PRODUCTS_UPDATE({ commit }, products) {
  //   commit(TYPES.PRODUCTS_UPDATE, products)
  // }
}

export default {
  mutations,
  actions,
  state
}
