export default {
  csrfToken: '',
  messages: {
    success: '',
    error: [],
    warning: '',
    validation: {}
  },
  fetching: false
}
