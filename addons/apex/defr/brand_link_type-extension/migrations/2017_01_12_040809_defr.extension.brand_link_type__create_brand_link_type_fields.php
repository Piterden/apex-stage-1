<?php

use Defr\CatalogModule\Brand\BrandModel;
use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrExtensionBrandLinkTypeCreateBrandLinkTypeFields
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DefrExtensionBrandLinkTypeCreateBrandLinkTypeFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'title'       => 'anomaly.field_type.text',
        'description' => 'anomaly.field_type.textarea',
        'brand'       => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => BrandModel::class,
            ],
        ],
    ];
}
