<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

/**
 * Class DefrExtensionBrandLinkTypeCreateBrandLinksStream
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DefrExtensionBrandLinkTypeCreateBrandLinksStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'brand_links',
        'title_column' => 'title',
        'translatable' => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'title'       => [
            'translatable' => true,
        ],
        'description' => [
            'translatable' => true,
        ],
        'brand'       => [
            'required' => true,
        ],
    ];

}
