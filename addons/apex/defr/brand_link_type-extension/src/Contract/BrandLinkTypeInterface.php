<?php namespace Defr\BrandLinkTypeExtension\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface BrandLinkTypeInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface BrandLinkTypeInterface extends EntryInterface
{

    /**
     * Get the related brand.
     *
     * @return EntryInterface
     */
    public function getBrand();
}
