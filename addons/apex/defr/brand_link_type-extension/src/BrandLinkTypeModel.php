<?php namespace Defr\BrandLinkTypeExtension;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\BrandLinkTypeExtension\Contract\BrandLinkTypeInterface;
use Anomaly\Streams\Platform\Model\BrandLinkType\BrandLinkTypeBrandLinksEntryModel;

/**
 * Class BrandLinkTypeModel
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class BrandLinkTypeModel extends BrandLinkTypeBrandLinksEntryModel implements BrandLinkTypeInterface
{

    /**
     * Eager load these relations.
     *
     * @var array
     */
    protected $with = [
        'translations',
    ];

    /**
     * Get the brand.
     *
     * @return BrandInterface
     */
    public function getBrand()
    {
        return $this->brand;
    }
}
