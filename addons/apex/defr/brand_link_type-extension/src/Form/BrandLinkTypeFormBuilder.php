<?php namespace Defr\BrandLinkTypeExtension\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class BrandLinkTypeFormBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class BrandLinkTypeFormBuilder extends FormBuilder
{

}
