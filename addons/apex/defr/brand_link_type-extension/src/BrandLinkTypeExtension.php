<?php namespace Defr\BrandLinkTypeExtension;

// use Anomaly\Streams\Platform\Addon\Extension\Extension;
use Anomaly\NavigationModule\Link\Contract\LinkInterface;
use Anomaly\NavigationModule\Link\Type\LinkTypeExtension;
use Defr\BrandLinkTypeExtension\Form\BrandLinkTypeFormBuilder;

class BrandLinkTypeExtension extends LinkTypeExtension
{

    /**
     * This extension provides...
     *
     * @var null|string
     */
    protected $provides = 'anomaly.module.navigation::link_type.brand';

    /**
     * Return the brand URL.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function url(LinkInterface $link)
    {
        /* @var BrandLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $relationship */
        if (!$brand = $entry->getBrand())
        {
            return url('');
        }

        return url($brand->getPath());
    }

    /**
     * Return the entry title.
     *
     * @param  LinkInterface $link
     * @return string
     */
    public function title(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $brand */
        if (!$brand = $entry->getBrand())
        {
            return '[Broken Link]';
        }

        return $brand->getTitle();
    }

    /**
     * Return if the link exists or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function exists(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        return (bool) $entry->getBrand();
    }

    /**
     * Return if the link is enabled or not.
     *
     * @param  LinkInterface $link
     * @return bool
     */
    public function enabled(LinkInterface $link)
    {
        /* @var CatalogLinkTypeModel $entry */
        $entry = $link->getEntry();

        /* @var EntryInterface $brand */
        if ($brand = $entry->getBrand())
        {
            return $brand->isEnabled();
        }

        return false;
    }

    /**
     * Return the form builder for the link type entry.
     *
     * @return FormBuilder
     */
    public function builder()
    {
        return app(BrandLinkTypeFormBuilder::class);
    }

}
